package cn.ljobin.bibi.api;

import cn.ljobin.bibi.model.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @program: guns
 * @description: 开方
 * @author: Mr.Liu
 * @create: 2019-09-12 17:28
 **/

public interface IkfService {
    /**
     * 添加新病人信息
     * @param patientInfo
     * @return
     */
    public int insertPatientInfo(@Param("Patient") Patient_info patientInfo);
    /**
     * 开方
     * @param kf
     * @param  neiRongList
     */
    public int insertKf(@Param("kfInfo") Kf kf, @Param("neiRongList") List<MStream> neiRongList, @Param("advise") Advise advise);
    /**
     * 开方 升级版
     * @param prescription
     * @param  neiRongList
     */
    public int insertKf2(PatientPrescription prescription, @Param("neiRongList") List<MStream> neiRongList, @Param("advise") Advise advise);
    /**
     * 根据药房id获取其所有的药类型
     * @param yao_room
     * @return
     */
    public List<MAddress> selectYao(Integer yao_room);
    /**
     * 根据病人id获取其所有基本信息
     * @param id
     * @return
     */
    public Patient_info selectPatientById(Long id);
    /**
     * 根据病人id获取其所有 全部信息
     * @param id
     * @return
     */
    public Patient_info selectPatientAllInfoById(Long id);
    /**
     * 更新病人信息
     * @param patient
     * @return
     */
    public int updatePatientInfo(Patient_info patient);
    /**
     * 获取所有已开药方
     * @return
     */
    public List<Kf> kfInfo();

    /**
     * 根据病人id获取其所有已开药方
     * @param id
     * @return
     */
    public List<PatientPrescription> kfInfoById(@Param("id") Long id, @Param("did") Long did);
    /**
     * 根据病人id获取其所有已开药方 【管理员】
     * @param id
     * @return
     */
    public PatientPrescription kfInfoByKfId(@Param("id") String id);
    /**
     * 根据病人id获取其所有已开药方 【管理员】
     * @param id
     * @return
     */
    public List<PatientPrescription> kfInfoById_root(@Param("id") Long id);
    /**
     * 获取所有病人 当前医师的
     * @return
     */
    public List<Patient_info> patient_info(@Param("did") Long did);
    /**
     * 获取所有病人 [管理员]
     * @return
     */
    public List<Patient_info> patient_info_root();
    /**
     * 根据开方单号获取开方 药品数据
     * @param kfid
     * @return
     */
    public List<MStream> yaoGetByKid(@Param("kfid") String kfid);
    /**
     * 根据开方单号获取开方 基础数据
     * @param kfid
     * @returnyaoGetByKid
     */
    public Kf kfGetByKid(@Param("kfid") String kfid);
    /**
     * 根据药方id获取其所有信息 升级版
     * @param kfId
     * @return
     */
    public PatientPrescription selectPatientKfByKfId(String kfId);
    /**
     * 根据开方单号获取医嘱数据
     * @param kfid
     * @return
     */
    public Advise adviseGetByKid(@Param("kfid") String kfid);
    /**
     * 批量更新中药库存数据
     * @param list
     * @return
     */
    public int updateMAddressList(@Param("list") List<MAddress> list);

    /**
     * 批量更新中药库存数据 map
     * @param all
     * @return
     */
    public int updataMAddressHshMap(@Param("all") ConcurrentHashMap<String, String> all);
    /**
     * 取药 获取药单开药数据，包括位置数据
     * @param kfid
     * @return
     */
    public List<MStream> getMedicine(@Param("kfid") String kfid);
    /**
     *更新药品取药状态
     * @param ids
     * @return
     */
    public int updateAllMedicineVersion(List<Integer> ids, String kfid);

    /**
     * 流式取药数据
     * @return
     */
    public Map<String,List<BoxContent>> getAllMA();
    /**
     * 根据药房id 获取其开始的地方
     * @param yaoRoomId
     * @return
     */
    public String selectYaoRoomBeginPath(@Param("yaoRoomId") Integer yaoRoomId);
    /**
     * 根据 药房 id 获取其所有柜子大小数据与其对应的药柜名称
     * @param yaoRoomId
     * @return
     */
    public List<KfBox> selectKfBox(@Param("yaoRoomId") Integer yaoRoomId);
    /**
     *  获取所有未进行路径规划的 开方单id
     * @return 对应的开方单id
     */
    public List<String> selectAllKfNoPath();
    /**
     *  获取所有未进行路径规划的 开方单id 升级版
     * @return 对应的开方单id
     */
    public List<String> selectAllKfNoPath2();
    /**
     *  根据kfid 获取对应的用户名称
     * @param kfid
     * @return
     */
    public String selectNameByKfId(@Param("kfid") String kfid);
    /**
     *  根据kfid 获取对应的用户名称 升级版
     * @param kfid
     * @return
     */
    public String selectNameByKfId2(@Param("kfid") String kfid);
    /**
     * 获取药方的药方路径规划起点
     * @param yao_room
     * @return
     */
    public String selectBeginPath(@Param("yao_room") int yao_room);

    /**
     * 修改药方的药方路径规划起点
     * @param yao_room
     * @param mr_address
     * @return
     */
    public int updateBeginPath(@Param("yao_room") int yao_room, @Param("mr_address") String mr_address);
}
