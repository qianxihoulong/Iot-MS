package cn.ljobin.bibi.model;


import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author ljobin
 * @since 2019-09-12
 */
public class YaoRoomKf {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private String kid;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKid() {
        return kid;
    }

    public void setKid(String kid) {
        this.kid = kid;
    }


    @Override
    public String toString() {
        return "YaoRoomKf{" +
        "id=" + id +
        ", kid=" + kid +
        "}";
    }
}
