package cn.ljobin.bibi.model;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author ljobin
 * @since 2019-09-12
 */
@Data
public class Kf {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private String name;
    private String sex;
    private Integer age;
    private Float moon;
    private String kfTime;
    private String address;
    private String diagnosis;
    private String kfid;
    private Long did;
    private char tag;
    private String cash;

    private Advise advise;

    public Advise getAdvise() {
        return advise;
    }

    public void setAdvise(Advise advise) {
        this.advise = advise;
    }

    public String getCash() {
        return cash;
    }

    public void setCash(String cash) {
        this.cash = cash;
    }

    private List<MStream> mStreams;

    public char getTag() {
        return tag;
    }

    public void setTag(char tag) {
        this.tag = tag;
    }

    public List<MStream> getmStreams() {
        return mStreams;
    }

    public void setmStreams(List<MStream> mStreams) {
        this.mStreams = mStreams;
    }

    public Kf() {
    }

    public Kf(String name, String sex, Integer age, Float moon, String kfTime, String address, String diagnosis, String mid, Long did) {
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.moon = moon;
        this.kfTime = kfTime;
        this.address = address;
        this.diagnosis = diagnosis;
        this.kfid = mid;
        this.did = did;
    }
    public Kf(String name, String sex, Integer age, Float moon, String kfTime, String address, String diagnosis, String mid, Long did,String cash) {
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.moon = moon;
        this.kfTime = kfTime;
        this.address = address;
        this.diagnosis = diagnosis;
        this.kfid = mid;
        this.did = did;
        this.cash = cash;
    }


    @Override
    public String toString() {
        return "Kf{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", age=" + age +
                ", moon=" + moon +
                ", kfTime='" + kfTime + '\'' +
                ", address='" + address + '\'' +
                ", diagnosis='" + diagnosis + '\'' +
                ", kfid='" + kfid + '\'' +
                ", did=" + did +
                ", tag=" + tag +
                ", cash='" + cash + '\'' +
                ", mStreams=" + mStreams +
                '}';
    }
}
