package cn.ljobin.bibi.model;


import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author ljobin
 * @since 2019-09-12
 */
public class YaoRoomDoctor{

    private static final long serialVersionUID = 1L;

    private Integer id;
    private String dName;
    private String dUid;
    private String dPwd;
    private String dSalt;
    private String picture;
    private String phone;
    private String enable;
    private Integer yaoRoomId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getdName() {
        return dName;
    }

    public void setdName(String dName) {
        this.dName = dName;
    }

    public String getdUid() {
        return dUid;
    }

    public void setdUid(String dUid) {
        this.dUid = dUid;
    }

    public String getdPwd() {
        return dPwd;
    }

    public void setdPwd(String dPwd) {
        this.dPwd = dPwd;
    }

    public String getdSalt() {
        return dSalt;
    }

    public void setdSalt(String dSalt) {
        this.dSalt = dSalt;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEnable() {
        return enable;
    }

    public void setEnable(String enable) {
        this.enable = enable;
    }

    public Integer getYaoRoomId() {
        return yaoRoomId;
    }

    public void setYaoRoomId(Integer yaoRoomId) {
        this.yaoRoomId = yaoRoomId;
    }


    @Override
    public String toString() {
        return "YaoRoomDoctor{" +
        "id=" + id +
        ", dName=" + dName +
        ", dUid=" + dUid +
        ", dPwd=" + dPwd +
        ", dSalt=" + dSalt +
        ", picture=" + picture +
        ", phone=" + phone +
        ", enable=" + enable +
        ", yaoRoomId=" + yaoRoomId +
        "}";
    }
}
