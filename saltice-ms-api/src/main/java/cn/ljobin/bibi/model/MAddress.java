package cn.ljobin.bibi.model;


import java.io.Serializable;

/**
 * <p>
 * ҩƷλ
 * </p>
 *
 * @author ljobin
 * @since 2019-09-12
 */
public class MAddress {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private String mName;
    private String mAddress;
    private Integer yaoRoom;
    private float mLiange;

    public MAddress(Integer id, String mName, String mAddress, Integer yaoRoom,float mLiange) {
        this.id = id;
        this.mName = mName;
        this.mAddress = mAddress;
        this.yaoRoom = yaoRoom;
        this.mLiange = mLiange;
    }

    public MAddress() {
    }

    public MAddress(String mName, String mAddress, Integer yaoRoom,float mLiange) {
        this.mName = mName;
        this.mAddress = mAddress;
        this.yaoRoom = yaoRoom;
        this.mLiange = mLiange;
    }
    public MAddress(Integer id, String mName, String mAddress,float mLiange) {
        this.id = id;
        this.mName = mName;
        this.mAddress = mAddress;
        this.mLiange = mLiange;
    }

    public float getmLiange() {
        return mLiange;
    }

    public void setmLiange(float mLiange) {
        this.mLiange = mLiange;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmAddress() {
        return mAddress;
    }

    public void setmAddress(String mAddress) {
        this.mAddress = mAddress;
    }


    public Integer getYaoRoom() {
        return yaoRoom;
    }

    public void setYaoRoom(Integer yaoRoom) {
        this.yaoRoom = yaoRoom;
    }


    @Override
    public String toString() {
        return "MAddress{" +
                "id=" + id +
                ", mName='" + mName + '\'' +
                ", mAddress='" + mAddress + '\'' +
                ", yaoRoom=" + yaoRoom +
                ", mLiange='" + mLiange + '\'' +
                '}';
    }
}
