package cn.ljobin.bibi.model;

import lombok.Data;

import java.util.List;

/**
 * @program: guns
 * @description: 病人信息
 * @author: Mr.Liu
 * @create: 2019-11-10 17:37
 **/
@Data
public class Patient_info {
    int id;
    String name;
    String sex;
    String birthday;
    String address;
    int height;
    int weight;
    Long did;
    String phone;
    PatientPrescription patientPrescription;
    Advise advise;
    List<MStream> mStreams;
    public Patient_info(int id, String name, String sex, String birthday, String address, int height, int weight,
                        Long did, String phone) {
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.birthday = birthday;
        this.address = address;
        this.height = height;
        this.weight = weight;
        this.did = did;
        this.phone = phone;
    }

    public Patient_info(String name, String sex, String birthday, String address, int height, int weight, Long did) {
        this.name = name;
        this.sex = sex;
        this.birthday = birthday;
        this.address = address;
        this.height = height;
        this.weight = weight;
        this.did = did;
    }
    public Patient_info(String name, String sex, String birthday, String address, int height, int weight, Long did,String phone) {
        this.name = name;
        this.sex = sex;
        this.birthday = birthday;
        this.address = address;
        this.height = height;
        this.weight = weight;
        this.did = did;
        this.phone=phone;
    }
    public Patient_info(int id, String name, String sex, String birthday, String address, int height, int weight) {
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.birthday = birthday;
        this.address = address;
        this.height = height;
        this.weight = weight;
    }

    public Patient_info(int id, String name, String sex, String birthday, String address, int height, int weight, Long did) {
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.birthday = birthday;
        this.address = address;
        this.height = height;
        this.weight = weight;
        this.did = did;
    }


}
