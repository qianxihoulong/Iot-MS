package cn.ljobin.bibi.domain.ms;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 【请填写功能名称】对象 yao_room
 * 
 * @author lyb
 * @date 2020-04-26
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@TableName("yao_room")
public class YaoRoom
{

    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /** $column.columnComment */
    private String yaoRoomName;

    /** $column.columnComment */
    private String legalPersonName;

    /** $column.columnComment */
    private String legalPersonPhone;

    /** $column.columnComment */
    private String yaoRoomAddress;

}
