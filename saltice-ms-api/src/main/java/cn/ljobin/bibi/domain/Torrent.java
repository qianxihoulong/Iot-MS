package cn.ljobin.bibi.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;
import java.util.List;

/**
 * 终端运转表 torrent
 * 
 * @author 小蔡
 * @date 2019-03-03
 */
@TableName("tb_ioterminal")
public class Torrent
{
	@TableId(value = "id",type = IdType.AUTO)
	private Long id;

	private String imei;

	private String ctype;

	private String conip;

	private String status;

	private Date startdate;
	@TableField(exist = false)
	private String beginStartdate;
	@TableField(exist = false)
	private String endStartdate;


	private Date exdate;
	@TableField(exist = false)
	private String beginExdate;
	@TableField(exist = false)
	private String endExdate;

	private Long deptid;

	private Long uid;

	private String demo;

	private String delflag;

	@TableField(exist = false)
	private String user_name;
	/**在线状态**/
	private String online;
	/**最后通信时间**/
	private Date enddate;


	public String getBeginStartdate() {
		return beginStartdate;
	}

	public void setBeginStartdate(String beginStartdate) {
		this.beginStartdate = beginStartdate;
	}

	public String getEndStartdate() {
		return endStartdate;
	}

	public void setEndStartdate(String endStartdate) {
		this.endStartdate = endStartdate;
	}

	public String getBeginExdate() {
		return beginExdate;
	}

	public void setBeginExdate(String beginExdate) {
		this.beginExdate = beginExdate;
	}

	public String getEndExdate() {
		return endExdate;
	}

	public void setEndExdate(String endExdate) {
		this.endExdate = endExdate;
	}

	public String getOnline() {
		return online;
	}

	public void setOnline(String online) {
		this.online = online;
	}

	public Date getEnddate() {
		return enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getCtype() {
		return ctype;
	}

	public void setCtype(String ctype) {
		this.ctype = ctype;
	}

	public String getConip() {
		return conip;
	}

	public void setConip(String conip) {
		this.conip = conip;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getStartdate() {
		return startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public Date getExdate() {
		return exdate;
	}

	public void setExdate(Date exdate) {
		this.exdate = exdate;
	}

	public Long getDeptid() {
		return deptid;
	}

	public void setDeptid(Long deptid) {
		this.deptid = deptid;
	}

	public Long getUid() {
		return uid;
	}

	public void setUid(long uid) {
		this.uid = uid;
	}

	public String getDemo() {
		return demo;
	}

	public void setDemo(String demo) {
		this.demo = demo;
	}

	public String getDelflag() {
		return delflag;
	}

	public void setDelflag(String delflag) {
		this.delflag = delflag;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	@Override
	public String toString() {
		return new StringBuilder()
				.append("id = "+getId())
				.append("imei = "+getImei())
				.append("ctype = "+getCtype())
				.append("conip = "+getConip())
				.append("status = "+getStatus())
				.append("startdate = "+getStartdate())
				.append("exdate = "+getExdate())
				.append("deptid = "+ getDeptid())
				.append("uid = "+getUid())
				.append("demo = "+getDemo())
				.append("delflag = "+getDelflag())
				.append("user_name = "+getUser_name())
				.toString();
	}
}
