package cn.ljobin.bibi.domain.ms;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 柜名对象 box
 * 
 * @author lyb
 * @date 2020-04-26
 */
@TableName("box")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Box
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /** 名称 */
    private String bName;

}
