package cn.ljobin.bibi.domain.ms;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 【请填写功能名称】对象 kf
 * 
 * @author lyb
 * @date 2020-04-26
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@TableName("kf")
public class Kf
{

    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /** $column.columnComment */
    private String name;

    /** $column.columnComment */
    private String sex;

    /** 年龄 */
    private Long age;

    /** 年龄月份 */
    private Long moon;

    /** 开方时间 */
    private String kfTime;

    /** 病人住址 */
    private String address;

    /** 诊断 */
    private String diagnosis;

    /** 药方编号 */
    private String kfid;

    /** 医师编号 */
    private Long did;

    /** 是否取完药 0:未取，1取完 */
    private String tag;

    /** 钱 */
    private String cash;

    private Advise advise;
}
