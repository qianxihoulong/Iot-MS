package cn.ljobin.bibi.domain.ms;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 每个药柜的大小对象 box_x_y
 * 
 * @author lyb
 * @date 2020-04-26
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@TableName("box_x_y")
public class BoxXY
{

    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /** 药房编号 */
    private Long yaoRoom;

    /** 柜子 x 长度 */
    private Long rX;

    /** 柜子 y长度 */
    private Long rY;

    /** 柜子名称编号 */
    private String boxName;
}
