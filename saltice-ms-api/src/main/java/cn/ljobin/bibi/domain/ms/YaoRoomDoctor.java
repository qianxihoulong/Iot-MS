package cn.ljobin.bibi.domain.ms;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 【请填写功能名称】对象 yao_room_doctor
 * 
 * @author lyb
 * @date 2020-04-26
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@TableName("yao_room_doctor")
public class YaoRoomDoctor
{

    /** $column.columnComment */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /** $column.columnComment */
    private String dName;

    /** $column.columnComment */
    private String dUid;

    /** $column.columnComment */
    private String dPwd;

    /** $column.columnComment */
    private String dSalt;

    /** $column.columnComment */
    private String picture;

    /** $column.columnComment */
    private String phone;

    /** $column.columnComment */
    private String enable;

    /** $column.columnComment */
    private Long yaoRoomId;
}
