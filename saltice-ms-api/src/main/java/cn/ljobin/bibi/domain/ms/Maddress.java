package cn.ljobin.bibi.domain.ms;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 【请填写功能名称】对象 maddress
 * 
 * @author lyb
 * @date 2020-04-26
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@TableName("MAddrress")
public class Maddress
{

    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /** $column.columnComment */
    private String mName;

    /** $column.columnComment */
    private String mAddress;

    /** 库存 单位/kg */
    private Long mLiang;

    /** $column.columnComment */
    private Long yaoRoom;
}
