package cn.ljobin.bibi.mapper;



import cn.ljobin.bibi.domain.Torrent;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.mybatis.spring.annotation.MapperScan;

import java.util.List;

/**
 * 终端运转 数据层
 * 
 * @author 刘衍斌
 * @date 2019-03-03
 */

@Mapper
public interface TorrentMapper extends BaseMapper<Torrent> {
    /**
     * 终端设备上下线
     * @param torrent
     * @return
     */
    public int onlineOrDisOnline(Torrent torrent);
}