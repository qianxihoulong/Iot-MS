package cn.ljobin.bibi.mapper;

import cn.ljobin.bibi.domain.ms.YaoRoom;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author lyb
 * @date 2020-04-26
 */
public interface YaoRoomMapper extends BaseMapper<YaoRoom>
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public YaoRoom selectYaoRoomById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param yaoRoom 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<YaoRoom> selectYaoRoomList(YaoRoom yaoRoom);

    /**
     * 新增【请填写功能名称】
     * 
     * @param yaoRoom 【请填写功能名称】
     * @return 结果
     */
    public int insertYaoRoom(YaoRoom yaoRoom);

    /**
     * 修改【请填写功能名称】
     * 
     * @param yaoRoom 【请填写功能名称】
     * @return 结果
     */
    public int updateYaoRoom(YaoRoom yaoRoom);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteYaoRoomById(Long id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteYaoRoomByIds(String[] ids);
}
