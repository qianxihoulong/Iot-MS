package cn.ljobin.bibi.mapper;


import cn.ljobin.bibi.domain.Energy;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
@DS("slave1")
public interface EnergyMapper extends BaseMapper<Energy> {

}