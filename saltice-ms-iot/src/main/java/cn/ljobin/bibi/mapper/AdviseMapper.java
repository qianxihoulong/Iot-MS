package cn.ljobin.bibi.mapper;


import cn.ljobin.bibi.domain.ms.Advise;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 医嘱信息Mapper接口
 * 
 * @author lyb
 * @date 2020-04-26
 */
@Mapper
public interface AdviseMapper extends BaseMapper<Advise>
{
    /**
     * 查询医嘱信息
     * 
     * @param id 医嘱信息ID
     * @return 医嘱信息
     */
    public Advise selectAdviseById(Long id);

    /**
     * 查询医嘱信息列表
     * 
     * @param advise 医嘱信息
     * @return 医嘱信息集合
     */
    public List<Advise> selectAdviseList(Advise advise);

    /**
     * 新增医嘱信息
     * 
     * @param advise 医嘱信息
     * @return 结果
     */
    public int insertAdvise(Advise advise);

    /**
     * 修改医嘱信息
     * 
     * @param advise 医嘱信息
     * @return 结果
     */
    public int updateAdvise(Advise advise);

    /**
     * 删除医嘱信息
     * 
     * @param id 医嘱信息ID
     * @return 结果
     */
    public int deleteAdviseById(Long id);

    /**
     * 批量删除医嘱信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteAdviseByIds(String[] ids);
}
