package cn.ljobin.bibi.service.Impl;

import java.util.List;

import cn.hutool.core.convert.Convert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.ljobin.bibi.mapper.YaoRoomMapper;
import cn.ljobin.bibi.domain.ms.YaoRoom;
import cn.ljobin.bibi.service.IYaoRoomService;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author lyb
 * @date 2020-04-26
 */
@Service
public class YaoRoomServiceImpl implements IYaoRoomService 
{
    @Autowired
    private YaoRoomMapper yaoRoomMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public YaoRoom selectYaoRoomById(Long id)
    {
        return yaoRoomMapper.selectYaoRoomById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param yaoRoom 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<YaoRoom> selectYaoRoomList(YaoRoom yaoRoom)
    {
        return yaoRoomMapper.selectYaoRoomList(yaoRoom);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param yaoRoom 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertYaoRoom(YaoRoom yaoRoom)
    {
        return yaoRoomMapper.insertYaoRoom(yaoRoom);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param yaoRoom 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateYaoRoom(YaoRoom yaoRoom)
    {
        return yaoRoomMapper.updateYaoRoom(yaoRoom);
    }

    /**
     * 删除【请填写功能名称】对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteYaoRoomByIds(String ids)
    {
        return yaoRoomMapper.deleteYaoRoomByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteYaoRoomById(Long id)
    {
        return yaoRoomMapper.deleteYaoRoomById(id);
    }
}
