package cn.ljobin.bibi.service.Impl;

import java.util.List;

import cn.hutool.core.convert.Convert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.ljobin.bibi.mapper.YaoRoomDoctorMapper;
import cn.ljobin.bibi.domain.ms.YaoRoomDoctor;
import cn.ljobin.bibi.service.IYaoRoomDoctorService;


/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author lyb
 * @date 2020-04-26
 */
@Service
public class YaoRoomDoctorServiceImpl implements IYaoRoomDoctorService 
{
    @Autowired
    private YaoRoomDoctorMapper yaoRoomDoctorMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public YaoRoomDoctor selectYaoRoomDoctorById(Long id)
    {
        return yaoRoomDoctorMapper.selectYaoRoomDoctorById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param yaoRoomDoctor 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<YaoRoomDoctor> selectYaoRoomDoctorList(YaoRoomDoctor yaoRoomDoctor)
    {
        return yaoRoomDoctorMapper.selectYaoRoomDoctorList(yaoRoomDoctor);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param yaoRoomDoctor 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertYaoRoomDoctor(YaoRoomDoctor yaoRoomDoctor)
    {
        return yaoRoomDoctorMapper.insertYaoRoomDoctor(yaoRoomDoctor);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param yaoRoomDoctor 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateYaoRoomDoctor(YaoRoomDoctor yaoRoomDoctor)
    {
        return yaoRoomDoctorMapper.updateYaoRoomDoctor(yaoRoomDoctor);
    }

    /**
     * 删除【请填写功能名称】对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteYaoRoomDoctorByIds(String ids)
    {
        return yaoRoomDoctorMapper.deleteYaoRoomDoctorByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteYaoRoomDoctorById(Long id)
    {
        return yaoRoomDoctorMapper.deleteYaoRoomDoctorById(id);
    }
}
