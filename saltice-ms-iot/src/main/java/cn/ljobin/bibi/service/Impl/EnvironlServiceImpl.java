package cn.ljobin.bibi.service.Impl;

import cn.ljobin.bibi.domain.Environl;
import cn.ljobin.bibi.mapper.EnvironlMapper;
import cn.ljobin.bibi.service.IEnvironlService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class EnvironlServiceImpl extends ServiceImpl<EnvironlMapper, Environl> implements IEnvironlService {

    @Override
    public boolean insert(Environl environl) {
        return this.save(environl);
    }
}