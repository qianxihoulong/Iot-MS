package cn.ljobin.bibi.service;

import cn.ljobin.bibi.domain.ms.Kf;
import java.util.List;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author lyb
 * @date 2020-04-26
 */
public interface IKfService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public Kf selectKfById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param kf 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<Kf> selectKfList(Kf kf);

    /**
     * 新增【请填写功能名称】
     * 
     * @param kf 【请填写功能名称】
     * @return 结果
     */
    public int insertKf(Kf kf);

    /**
     * 修改【请填写功能名称】
     * 
     * @param kf 【请填写功能名称】
     * @return 结果
     */
    public int updateKf(Kf kf);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteKfByIds(String ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteKfById(Long id);
}
