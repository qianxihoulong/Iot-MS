package cn.ljobin.bibi.service;

import cn.ljobin.bibi.domain.ms.Mr;
import java.util.List;

/**
 * 取药开始的地方，由用户自行设置Service接口
 * 
 * @author lyb
 * @date 2020-04-26
 */
public interface IMrService 
{
    /**
     * 查询取药开始的地方，由用户自行设置
     * 
     * @param id 取药开始的地方，由用户自行设置ID
     * @return 取药开始的地方，由用户自行设置
     */
    public Mr selectMrById(Long id);

    /**
     * 查询取药开始的地方，由用户自行设置列表
     * 
     * @param mr 取药开始的地方，由用户自行设置
     * @return 取药开始的地方，由用户自行设置集合
     */
    public List<Mr> selectMrList(Mr mr);

    /**
     * 新增取药开始的地方，由用户自行设置
     * 
     * @param mr 取药开始的地方，由用户自行设置
     * @return 结果
     */
    public int insertMr(Mr mr);

    /**
     * 修改取药开始的地方，由用户自行设置
     * 
     * @param mr 取药开始的地方，由用户自行设置
     * @return 结果
     */
    public int updateMr(Mr mr);

    /**
     * 批量删除取药开始的地方，由用户自行设置
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMrByIds(String ids);

    /**
     * 删除取药开始的地方，由用户自行设置信息
     * 
     * @param id 取药开始的地方，由用户自行设置ID
     * @return 结果
     */
    public int deleteMrById(Long id);
}
