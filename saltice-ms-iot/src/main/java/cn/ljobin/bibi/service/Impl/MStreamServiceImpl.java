package cn.ljobin.bibi.service.Impl;

import java.util.List;

import cn.hutool.core.convert.Convert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.ljobin.bibi.mapper.MStreamMapper;
import cn.ljobin.bibi.domain.ms.MStream;
import cn.ljobin.bibi.service.IMStreamService;


/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author lyb
 * @date 2020-04-26
 */
@Service
public class MStreamServiceImpl implements IMStreamService 
{
    @Autowired
    private MStreamMapper mStreamMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public MStream selectMStreamById(Long id)
    {
        return mStreamMapper.selectMStreamById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param mStream 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<MStream> selectMStreamList(MStream mStream)
    {
        return mStreamMapper.selectMStreamList(mStream);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param mStream 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertMStream(MStream mStream)
    {
        return mStreamMapper.insertMStream(mStream);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param mStream 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateMStream(MStream mStream)
    {
        return mStreamMapper.updateMStream(mStream);
    }

    /**
     * 删除【请填写功能名称】对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMStreamByIds(String ids)
    {
        return mStreamMapper.deleteMStreamByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteMStreamById(Long id)
    {
        return mStreamMapper.deleteMStreamById(id);
    }
}
