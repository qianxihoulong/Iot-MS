package cn.ljobin.bibi.service.Impl;

import java.util.List;

import cn.hutool.core.convert.Convert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.ljobin.bibi.mapper.PatientInfoMapper;
import cn.ljobin.bibi.domain.ms.PatientInfo;
import cn.ljobin.bibi.service.IPatientInfoService;

/**
 * 病人信息Service业务层处理
 * 
 * @author lyb
 * @date 2020-04-26
 */
@Service
public class PatientInfoServiceImpl implements IPatientInfoService 
{
    @Autowired
    private PatientInfoMapper patientInfoMapper;

    /**
     * 查询病人信息
     * 
     * @param id 病人信息ID
     * @return 病人信息
     */
    @Override
    public PatientInfo selectPatientInfoById(Long id)
    {
        return patientInfoMapper.selectPatientInfoById(id);
    }

    /**
     * 查询病人信息列表
     * 
     * @param patientInfo 病人信息
     * @return 病人信息
     */
    @Override
    public List<PatientInfo> selectPatientInfoList(PatientInfo patientInfo)
    {
        return patientInfoMapper.selectPatientInfoList(patientInfo);
    }

    /**
     * 新增病人信息
     * 
     * @param patientInfo 病人信息
     * @return 结果
     */
    @Override
    public int insertPatientInfo(PatientInfo patientInfo)
    {
        return patientInfoMapper.insertPatientInfo(patientInfo);
    }

    /**
     * 修改病人信息
     * 
     * @param patientInfo 病人信息
     * @return 结果
     */
    @Override
    public int updatePatientInfo(PatientInfo patientInfo)
    {
        return patientInfoMapper.updatePatientInfo(patientInfo);
    }

    /**
     * 删除病人信息对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deletePatientInfoByIds(String ids)
    {
        return patientInfoMapper.deletePatientInfoByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除病人信息信息
     * 
     * @param id 病人信息ID
     * @return 结果
     */
    @Override
    public int deletePatientInfoById(Long id)
    {
        return patientInfoMapper.deletePatientInfoById(id);
    }
}
