package cn.ljobin.bibi.service.Impl;

import java.util.List;

import cn.hutool.core.convert.Convert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.ljobin.bibi.mapper.BoxMapper;
import cn.ljobin.bibi.domain.ms.Box;
import cn.ljobin.bibi.service.IBoxService;

/**
 * 柜名Service业务层处理
 * 
 * @author lyb
 * @date 2020-04-26
 */
@Service
public class BoxServiceImpl implements IBoxService 
{
    @Autowired
    private BoxMapper boxMapper;

    /**
     * 查询柜名
     * 
     * @param id 柜名ID
     * @return 柜名
     */
    @Override
    public Box selectBoxById(Long id)
    {
        return boxMapper.selectBoxById(id);
    }

    /**
     * 查询柜名列表
     * 
     * @param box 柜名
     * @return 柜名
     */
    @Override
    public List<Box> selectBoxList(Box box)
    {
        return boxMapper.selectBoxList(box);
    }

    /**
     * 新增柜名
     * 
     * @param box 柜名
     * @return 结果
     */
    @Override
    public int insertBox(Box box)
    {
        return boxMapper.insertBox(box);
    }

    /**
     * 修改柜名
     * 
     * @param box 柜名
     * @return 结果
     */
    @Override
    public int updateBox(Box box)
    {
        return boxMapper.updateBox(box);
    }

    /**
     * 删除柜名对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteBoxByIds(String ids)
    {
        return boxMapper.deleteBoxByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除柜名信息
     * 
     * @param id 柜名ID
     * @return 结果
     */
    @Override
    public int deleteBoxById(Long id)
    {
        return boxMapper.deleteBoxById(id);
    }
}
