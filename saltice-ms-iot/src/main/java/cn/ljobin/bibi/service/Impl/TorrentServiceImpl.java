package cn.ljobin.bibi.service.Impl;

import cn.ljobin.bibi.domain.Torrent;
import cn.ljobin.bibi.mapper.TorrentMapper;
import cn.ljobin.bibi.service.ITorrentService;
import cn.ljobin.bibi.utils.AnnotationUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 终端运转 服务层实现
 * 
 * @author lyb
 * @date 2019-03-03
 */
@Service
public class TorrentServiceImpl extends ServiceImpl<TorrentMapper,Torrent> implements ITorrentService {

    @Autowired
    private TorrentMapper torrentMapper;
    private volatile static AnnotationUtils<Torrent> ut;
    public AnnotationUtils<Torrent> getUt(){
        if(ut == null){
            synchronized (AnnotationUtils.class){
                if (ut == null){
                    ut = new AnnotationUtils<>();
                }
            }
        }
        return ut;
    }
    @Override
    public int onlineOrDisOnline(Torrent torrent) {
        return torrentMapper.onlineOrDisOnline(torrent);
    }

    @Override
    public List<Torrent> selectTorrentList(Torrent torrent) {
        QueryWrapper<Torrent> queryWrapper = new QueryWrapper<>();
        queryWrapper = getUt().wapperData(queryWrapper,torrent);
        return  this.list(queryWrapper);
    }
}
