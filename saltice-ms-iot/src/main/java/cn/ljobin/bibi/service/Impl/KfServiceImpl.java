package cn.ljobin.bibi.service.Impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import cn.hutool.core.convert.Convert;
import cn.ljobin.bibi.api.IkfService;
import cn.ljobin.bibi.model.*;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.ljobin.bibi.mapper.KfMapper;
import cn.ljobin.bibi.service.IKfService;


/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author lyb
 * @date 2020-04-26
 */
@Service
public class KfServiceImpl implements IkfService {
    @Autowired
    KfMapper kfMapper;

    @Override
    public int insertPatientInfo(Patient_info patientInfo) {
        return kfMapper.insertPatientInfo(patientInfo);
    }

    @Override
    public int insertKf(Kf kf, List<MStream> neiRongList, Advise advise) {
        return kfMapper.insertKf(kf,neiRongList,advise);
    }

    @Override
    public int insertKf2(PatientPrescription prescription, List<MStream> neiRongList, Advise advise) {
        return kfMapper.insertKf2(prescription,neiRongList,advise);
    }

    @Override
    public List<MAddress> selectYao(Integer yao_room) {
        return kfMapper.selectYao(yao_room);
    }

    @Override
    public Patient_info selectPatientById(Long id) {
        return kfMapper.selectPatientById(id);
    }

    @Override
    public Patient_info selectPatientAllInfoById(Long id) {
        return kfMapper.selectPatientAllInfoById(id);
    }

    @Override
    public int updatePatientInfo(Patient_info patient) {
        return kfMapper.updatePatientInfo(patient);
    }

    @Override
    public List<Kf> kfInfo() {
        return kfMapper.kfInfo();
    }

    @Override
    public List<Patient_info> patient_info( Long did) {
        return kfMapper.patient_info(did);
    }

    @Override
    public List<Patient_info> patient_info_root() {
        return kfMapper.patient_info_root();
    }

    @Override
    public List<PatientPrescription> kfInfoById(Long id,@Param("did") Long did) {
        return kfMapper.kfInfoById(id,did);
    }

    @Override
    public PatientPrescription kfInfoByKfId(String id) {
        return null;
    }

    @Override
    public List<PatientPrescription> kfInfoById_root(Long id) {
        return kfMapper.kfInfoById_root(id);
    }

    @Override
    public List<MStream> yaoGetByKid(String kfid) {
        return kfMapper.yaoGetByKid(kfid);
    }

    @Override
    public Kf kfGetByKid(String kfid) {
        return kfMapper.kfGetByKid(kfid);
    }

    @Override
    public PatientPrescription selectPatientKfByKfId(String kfId) {
        return kfMapper.selectPatientKfByKfId(kfId);
    }

    @Override
    public Advise adviseGetByKid(String kfid) {
        return kfMapper.adviseGetByKid(kfid);
    }

    /**
     * 批量更新中药库存数据
     * @param list
     * @return
     */
    @Override
    public int updateMAddressList(List<MAddress> list) {
        return kfMapper.updateMAddressList(list);
    }
    /**
     * 批量更新中药库存数据 map
     * @param all
     * @return
     */
    @Override
    public int updataMAddressHshMap(ConcurrentHashMap<String, String> all) {
        return kfMapper.updataMAddressHshMap(all);
    }
    /**
     * 取药 获取药单开药数据，包括位置数据
     * @param kfid
     * @return
     */
    @Override
    public List<MStream> getMedicine(String kfid) {
        return kfMapper.getMedicine(kfid);
    }

    /**
     *更新药品取药状态
     * @param ids
     * @return
     */
    @Override
    public int updateAllMedicineVersion(List<Integer> ids,String kfid) {
        return kfMapper.updateAllMedicineVersion(ids,kfid);
    }

    /**
     * 流式取药数据 药房 ：1
     * @return
     */
    @Override
    public Map<String, List<BoxContent>> getAllMA() {
        List<MAddress> list = kfMapper.selectYao(1);
        int size = list.size();
        List<BoxContent> boxContentList = new ArrayList<>();
        List<String> boxNameList = new ArrayList<>();
        Map<String, List<BoxContent>> stringListMap = new HashMap<>();
        for (int i = 0; i < size; i++) {
            //中药柜2-->第6行，第1列
            //药柜名称 中药柜2
            String boxName = list.get(i).getmAddress().split("-->")[0];
            // x 坐标
            String x = list.get(i).getmAddress().split("-->")[1].split("，")[0];
            String address_x = x.substring(1,x.length()-1);
            // y 坐标
            String y = list.get(i).getmAddress().split("-->")[1].split("，")[1];
            String address_y = y.substring(1,y.length()-1);
            BoxContent boxContent = new BoxContent(boxName,address_x,address_y,list.get(i).getmName());
            boxNameList.add(boxName);
            boxContentList.add(boxContent);
        }
        int boxNameListSize = boxNameList.size();
        for (int i=0;i<boxNameListSize;i++){
            List<BoxContent> list1 = new ArrayList<>();
            for (int j=0;j<size;j++){
                if(boxContentList.get(j).getBoxName().equals(boxNameList.get(i))){
                    list1.add(boxContentList.get(j));
                }
            }
            stringListMap.put(boxNameList.get(i),list1);
        }
        return stringListMap;
    }

    @Override
    public String selectYaoRoomBeginPath(Integer yaoRoomId) {
        return kfMapper.selectYaoRoomBeginPath(yaoRoomId);
    }

    @Override
    public List<KfBox> selectKfBox(Integer yaoRoomId) {
        return kfMapper.selectKfBox(yaoRoomId);
    }

    @Override
    public List<String> selectAllKfNoPath() {
        return kfMapper.selectAllKfNoPath();
    }

    @Override
    public List<String> selectAllKfNoPath2() {
        return kfMapper.selectAllKfNoPath2();
    }

    @Override
    public String selectNameByKfId(String kfid) {
        return kfMapper.selectNameByKfId(kfid);
    }

    @Override
    public String selectNameByKfId2(String kfid) {
        return kfMapper.selectNameByKfId2(kfid);
    }

    @Override
    public String selectBeginPath(int yao_room) {
        return kfMapper.selectBeginPath(yao_room);
    }

    @Override
    public int updateBeginPath(int yao_room, String mr_address) {
        return kfMapper.updateBeginPath(yao_room,mr_address);
    }
}
