package cn.ljobin.bibi.service;

import cn.ljobin.bibi.domain.ms.Box;
import java.util.List;

/**
 * 柜名Service接口
 * 
 * @author lyb
 * @date 2020-04-26
 */
public interface IBoxService 
{
    /**
     * 查询柜名
     * 
     * @param id 柜名ID
     * @return 柜名
     */
    public Box selectBoxById(Long id);

    /**
     * 查询柜名列表
     * 
     * @param box 柜名
     * @return 柜名集合
     */
    public List<Box> selectBoxList(Box box);

    /**
     * 新增柜名
     * 
     * @param box 柜名
     * @return 结果
     */
    public int insertBox(Box box);

    /**
     * 修改柜名
     * 
     * @param box 柜名
     * @return 结果
     */
    public int updateBox(Box box);

    /**
     * 批量删除柜名
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBoxByIds(String ids);

    /**
     * 删除柜名信息
     * 
     * @param id 柜名ID
     * @return 结果
     */
    public int deleteBoxById(Long id);
}
