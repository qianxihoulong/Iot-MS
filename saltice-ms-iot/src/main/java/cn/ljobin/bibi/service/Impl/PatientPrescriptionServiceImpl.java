package cn.ljobin.bibi.service.Impl;

import java.util.List;

import cn.hutool.core.convert.Convert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.ljobin.bibi.mapper.PatientPrescriptionMapper;
import cn.ljobin.bibi.domain.ms.PatientPrescription;
import cn.ljobin.bibi.service.IPatientPrescriptionService;


/**
 * 病人药方Service业务层处理
 * 
 * @author lyb
 * @date 2020-04-26
 */
@Service
public class PatientPrescriptionServiceImpl implements IPatientPrescriptionService 
{
    @Autowired
    private PatientPrescriptionMapper patientPrescriptionMapper;

    /**
     * 查询病人药方
     * 
     * @param id 病人药方ID
     * @return 病人药方
     */
    @Override
    public PatientPrescription selectPatientPrescriptionById(Long id)
    {
        return patientPrescriptionMapper.selectPatientPrescriptionById(id);
    }

    /**
     * 查询病人药方列表
     * 
     * @param patientPrescription 病人药方
     * @return 病人药方
     */
    @Override
    public List<PatientPrescription> selectPatientPrescriptionList(PatientPrescription patientPrescription)
    {
        return patientPrescriptionMapper.selectPatientPrescriptionList(patientPrescription);
    }

    /**
     * 新增病人药方
     * 
     * @param patientPrescription 病人药方
     * @return 结果
     */
    @Override
    public int insertPatientPrescription(PatientPrescription patientPrescription)
    {
        return patientPrescriptionMapper.insertPatientPrescription(patientPrescription);
    }

    /**
     * 修改病人药方
     * 
     * @param patientPrescription 病人药方
     * @return 结果
     */
    @Override
    public int updatePatientPrescription(PatientPrescription patientPrescription)
    {
        return patientPrescriptionMapper.updatePatientPrescription(patientPrescription);
    }

    /**
     * 删除病人药方对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deletePatientPrescriptionByIds(String ids)
    {
        return patientPrescriptionMapper.deletePatientPrescriptionByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除病人药方信息
     * 
     * @param id 病人药方ID
     * @return 结果
     */
    @Override
    public int deletePatientPrescriptionById(Long id)
    {
        return patientPrescriptionMapper.deletePatientPrescriptionById(id);
    }
}
