package cn.ljobin.bibi.service;

import cn.ljobin.bibi.domain.ms.BoxXY;
import java.util.List;

/**
 * 每个药柜的大小Service接口
 * 
 * @author lyb
 * @date 2020-04-26
 */
public interface IBoxXYService 
{
    /**
     * 查询每个药柜的大小
     * 
     * @param id 每个药柜的大小ID
     * @return 每个药柜的大小
     */
    public BoxXY selectBoxXYById(Long id);

    /**
     * 查询每个药柜的大小列表
     * 
     * @param boxXY 每个药柜的大小
     * @return 每个药柜的大小集合
     */
    public List<BoxXY> selectBoxXYList(BoxXY boxXY);

    /**
     * 新增每个药柜的大小
     * 
     * @param boxXY 每个药柜的大小
     * @return 结果
     */
    public int insertBoxXY(BoxXY boxXY);

    /**
     * 修改每个药柜的大小
     * 
     * @param boxXY 每个药柜的大小
     * @return 结果
     */
    public int updateBoxXY(BoxXY boxXY);

    /**
     * 批量删除每个药柜的大小
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBoxXYByIds(String ids);

    /**
     * 删除每个药柜的大小信息
     * 
     * @param id 每个药柜的大小ID
     * @return 结果
     */
    public int deleteBoxXYById(Long id);
}
