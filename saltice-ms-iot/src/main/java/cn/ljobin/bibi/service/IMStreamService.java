package cn.ljobin.bibi.service;

import cn.ljobin.bibi.domain.ms.MStream;
import java.util.List;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author lyb
 * @date 2020-04-26
 */
public interface IMStreamService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public MStream selectMStreamById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param mStream 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<MStream> selectMStreamList(MStream mStream);

    /**
     * 新增【请填写功能名称】
     * 
     * @param mStream 【请填写功能名称】
     * @return 结果
     */
    public int insertMStream(MStream mStream);

    /**
     * 修改【请填写功能名称】
     * 
     * @param mStream 【请填写功能名称】
     * @return 结果
     */
    public int updateMStream(MStream mStream);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMStreamByIds(String ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteMStreamById(Long id);
}
