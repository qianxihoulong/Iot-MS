package cn.ljobin.bibi.netty.decoder;


import cn.ljobin.bibi.netty.util.ByteUtil;
import cn.ljobin.bibi.netty.util.InputCommandUtil;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.Arrays;
import java.util.List;

/**
 * 保证每次ClientHandler每次接收到的数据都是一个完整的数据包。并将数据解析成16进制的字符串
 * create by zhaojing at 2019/5/22 15:03
 */
public class PacketDecoder extends ByteToMessageDecoder {
    private StringBuilder sb = new StringBuilder();
    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf in, List<Object> out) throws Exception {
        byte[] read =new byte[in.readableBytes()];
        in.readBytes(read);
        boolean clientTag = InputCommandUtil.isClientCmd(Arrays.toString(read));
        if(clientTag){
            System.err.println("接收到服务器客户端数据格式不对的数据"+Arrays.toString(read));
            out.add("");
            sb.setLength(0);
        }else {
            String packet = ByteUtil.BinaryToHexString(read).toUpperCase();
            //System.out.println("ByteToMessageDecoder "+packet);
            sb.append(packet);
            //判断是否是硬件上传的数据
            boolean masterTag = InputCommandUtil.isMasterCmd(packet);
            boolean slaveTag = InputCommandUtil.isSlaveCmd(packet);

            if (!masterTag && !slaveTag) {
                System.err.println("接收到数据格式不对的数据"+packet);
                out.add("");
                sb.setLength(0);
            }else {
                out.add(sb.toString());
                sb.setLength(0);
            }
        }


    }
}
