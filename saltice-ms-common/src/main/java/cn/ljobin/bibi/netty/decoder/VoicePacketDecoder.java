package cn.ljobin.bibi.netty.decoder;


import cn.ljobin.bibi.netty.util.ByteUtil;
import cn.ljobin.bibi.netty.util.InputComVoiceUtil;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

/**
 * 保证每次ClientHandler每次接收到的数据都是一个完整的数据包。并将数据解析成16进制的字符串
 * create by liuyanbin at 2019/10/21 13:02
 * @author liuyanbin
 */
public class VoicePacketDecoder extends ByteToMessageDecoder {
    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf in, List<Object> out) throws Exception {
        byte[] read =new byte[in.readableBytes()];
        in.readBytes(read);
        String packet = ByteUtil.BinaryToHexString(read).toUpperCase();
        if(InputComVoiceUtil.isOkAckCode(packet)){
            out.add(packet);
        }else if(InputComVoiceUtil.isEndAckCode(packet)){
            out.add(packet);
        }else {
            out.add(packet);
        }

    }
}
