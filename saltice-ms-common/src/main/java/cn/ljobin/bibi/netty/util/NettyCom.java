package cn.ljobin.bibi.netty.util;


import cn.ljobin.bibi.netty.client.Slave;
import cn.ljobin.bibi.netty.client.Voice;
import cn.ljobin.bibi.netty.voice.VoiceWriteAndFlush;
import cn.ljobin.bibi.utils.cache.Cache;
import io.netty.channel.Channel;

import java.util.ArrayList;
import java.util.List;

/**
 * netty处理串口数据
 * @author liuyanbin
 */
public class NettyCom {
    private Slave salve;
    private Voice voice;
    /**
     *
     * @param comName 串口名称如：COM14
     * @param baudrate 波特率 如：115200
     * @param uid 账号
     */
    public Slave StarNewCom(String comName,int baudrate,String uid){

        if(baudrate<0){
            System.err.println("波特率错误");
        }else {
            if(ComDataUtil.checkCom(comName)){
                //初始化150个寄存器空间，全部存0000，该空间存储16进制字符串
                List<String> list =new ArrayList<>();
                for (int i = 0; i < 150; i++) {
                    list.add("0000");
                }
                //设置串口名，波特率，和寄存器空间
                this.salve =new Slave(comName,baudrate,list,uid);
                this.salve.run();
                System.err.println(comName+"启动成功");
                Cache.put(comName,this.salve);
                return this.salve;
            }else {
                System.err.println("端口错误，可能正在使用或者无效");
            }
        }
        return this.salve;
    }
    /**
     *
     * @param comName 串口名称如：COM14
     * @param baudrate 波特率 如：115200
     * @param uid 账号
     */
    public Voice starVoiceCom(String comName,int baudrate,String uid){

        if(baudrate<0){
            System.err.println("波特率错误");
        }else {
            if(ComDataUtil.checkCom(comName)){

                //设置串口名，波特率，和寄存器空间
                this.voice =new Voice(comName,baudrate,uid);
                this.voice.run();
                System.err.println(comName+"：语音模块启动成功");
                Cache.put(comName,this.voice);
                return this.voice;
            }else {
                System.err.println("语音模块启动端口错误，可能正在使用或者无效");
            }
        }
        return this.voice;
    }
    public void writeDate(String hexString){
        this.salve.writeAndFlush(hexString);
    }

    public static void main( String[] args )
    {
        NettyCom nettyCom = new NettyCom();
        nettyCom.starVoiceCom("COM5",115200, "client");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Channel channel = (Channel) Cache.get("voiceChannel");
        if(channel != null && channel.isWritable()){
            VoiceWriteAndFlush.voiceGetStatus(channel);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            VoiceWriteAndFlush.voiceWriteAndFlush(channel,"我叫刘衍斌，我爱编程");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            VoiceWriteAndFlush.voiceGetStatus(channel);
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            VoiceWriteAndFlush.voiceSleep(channel);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            VoiceWriteAndFlush.voiceGetStatus(channel);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            VoiceWriteAndFlush.voiceWriteAndFlush(channel,"我叫刘衍斌，我爱编程");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            VoiceWriteAndFlush.voiceGetStatus(channel);
        }else {
            System.err.println("error");
        }
//        byte[] bytes = new byte[0];
//        try {
//            System.err.println(bytesToHexFun1("科大讯飞".getBytes("GB2312")));
//            bytes = ByteUtil.hexStringToBytes("FD000A0100"+bytesToHexFun1("科大讯飞".getBytes("GB2312")));
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
//        //byte[] bytes = ByteUtil.hexStringToBytes("FD000188");
//        //byte[] bytes = ByteUtil.hexStringToBytes("FD0001FF");
//        ByteBuf buffer = channel.alloc().buffer();
//        ByteBuf byteBuf = buffer.writeBytes(bytes);
//        channel.writeAndFlush(byteBuf).addListener(new ChannelFutureListener() {
//            @Override
//            public void operationComplete(ChannelFuture future) throws Exception {
//                StringBuilder sb = new StringBuilder("");
//                if (future.isSuccess()) {
//                    System.out.println(sb.toString() + "回写成功");
//                } else {
//                    System.out.println(sb.toString() + "回写失败");
//
//                }
//            }
//        });

        //发送数据给串口下的下位机
        //nettyCom.writeDate("06010803020132");
        //关闭函数
        //salve.stop();

        //Util.fillFloat(list,ByteUtil.hexStringToNum("00"),16*4,"0000");
        //ListUtil.fillBollean(list, ByteUtil.hexStringToNum("40"),16*3,0);
        //Util.fillFloat(list,ByteUtil.hexStringToNum("70"),16,"0000");

        /*
        //设值
        ListUtil.setFloat(list,"10",38.9f);
        ListUtil.setFloat(list,"20",39f);
        ListUtil.setFloat(list,"30",40f);
        ListUtil.setBoolean(list,"50",0);
        ListUtil.setBoolean(list,"60",0);
        ListUtil.setBoolean(list,"70",0);*/

        //salve.inputCmd();
        //主动向传感器发送请求命令,此该当作master端在用
        /*while (true){
            try {
                salve.request();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }*/

    }
public static String checkCode(String code){
    if(code.length()>65535){

    }
    return "";
}
    //将byte数组转成16进制字符串
    public static String bytesToHexFun1(byte[] bytes) {
        char[] HEX_CHAR = {'0', '1', '2', '3', '4', '5',
                '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        // 一个byte为8位，可用两个十六进制位标识
        char[] buf = new char[bytes.length * 2];
        int a = 0;
        int index = 0;
        for(byte b : bytes) { // 使用除与取余进行转换
            if(b < 0) {
                a = 256 + b;
            } else {
                a = b;
            }
            buf[index++] = HEX_CHAR[a / 16];
            buf[index++] = HEX_CHAR[a % 16];
        }
        return new String(buf);
    }
}
