package cn.ljobin.bibi.netty.handler;


import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.util.concurrent.atomic.AtomicInteger;

public class VoiceHandler extends ChannelInboundHandlerAdapter {
    private static AtomicInteger index = new AtomicInteger(0);
    private String uid = "";

    public static int getIndex() {
        return index.get();
    }

    public static void setIndex(int index) {
        VoiceHandler.index.set(index);
    }

    public VoiceHandler(String uid) {
        this.uid = uid;
    }


    /**
     * 接收到客户端数据的回调
     */
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception{
        if(msg instanceof String ){
            String packet = (String)msg;
            System.err.println("接收到语音模块数据："+packet);
        }else {
            System.err.println("接收到异常类型数据");
        }

    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }
}
