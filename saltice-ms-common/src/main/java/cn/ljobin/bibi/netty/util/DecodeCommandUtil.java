package cn.ljobin.bibi.netty.util;

public class DecodeCommandUtil {
    /**
     * 解析
     * @param msg
     * @return {String[]} {地址码，湿度，温度}
     */
    public static String[] decodeSWD(String msg){
        if(!InputCommandUtil.isSlaveCmd(msg)){
            return null;
        }

        String [] ret=new String [3];

        String addrCode = msg.substring(0, 2);
        ret[0]=addrCode;

        String lengthHexString = msg.substring(4, 6);
        int length = ByteUtil.hexStringToNum(lengthHexString)*2;
        int retIndex= 1;
        for (int i = 6; i < 6+length ; i+=4) {
            String substring = msg.substring(i, i + 4);
            Float aFloat = ByteUtil.hexStringToFloat2(substring);
            String hexString = ByteUtil.floatToHexString(aFloat);
            ret[retIndex]=hexString;
            retIndex++;
        }

        return ret;
    }

    /**
     * 解析水浸
     * @param msg
     * @return {String[]} {地址码，状态}
     */
    public static String[] decodeSoaking(String msg){
        if(!InputCommandUtil.isSlaveCmd(msg)){
            return null;
        }

        String [] ret=new String [2];

        String addrCode = msg.substring(0, 2);
        ret[0]=addrCode;
        //水浸数据是状态量，只取一个低节
        String val = msg.substring(6, 10);
        //由于水浸 1：正常，2：报警，这里做个转化，转化为 0：正常，1：报警
        int i = ByteUtil.hexStringToNum(val);

        ret[1] ="0"+i;

        return ret;
    }


    /**
     * 解析烟雾
     * @param msg
     * @return {String[]} {地址码，状态}
     */
    public static String[] decodeSmoking(String msg){
        if(!InputCommandUtil.isSlaveCmd(msg)){
            return null;
        }

        String [] ret=new String [2];

        String addrCode = msg.substring(0, 2);
        ret[0]=addrCode;
        //水浸数据是状态量，只取一个低节
        String val = msg.substring(8, 10);

        ret[1] = val;

        return ret;
    }

    /**
     * 上一个药
     */
    public static boolean isPreCode(String msg){
        return "03".equals(msg.substring(4,6));
    }
    /**
     * 下一个药
     */
    public static boolean isNextCode(String msg){
        return "02".equals(msg.substring(4,6));
    }
    /**
     * 下一个人
     */
    public static boolean isNextMan(String msg){
        return "04".equals(msg.substring(4,6));
    }
    public static boolean isSmoking(String msg){
       return "04".equals(msg.substring(0,2));
    }
    public static boolean isSoaking(String msg){
        return "02".equals(msg.substring(0,2));
    }
    public static boolean isWetTemperature(String msg){
        return "03".equals(msg.substring(0,2));
    }


    public static void main(String[] args) {
        String msg="03030402C200F9B9F5";
        decodeSWD(msg);
    }


}
