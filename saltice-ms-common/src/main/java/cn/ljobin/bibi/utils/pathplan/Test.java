package cn.ljobin.bibi.utils.pathplan;

/**
 * @program: guns
 * @description:
 * @author: Mr.Liu
 * @create: 2019-09-26 09:15
 **/
import java.util.Deque;

public class Test {
    public static void main(String[] args) {
        //初始化地图，横向x个节点，纵向y个节点，节点尺寸为1，起点为（0,0）
        AstarPathPlan a = new AstarPathPlan(20, 20, 1, 0, 0);
        //编辑障碍，中心（5,5）,半径3个节点
        //a.map.editObstacle(5, 5, 3);
        //a.map.editObstacle(8, 3, 2);
        //打印地图
        a.map.print();
        System.out.println();
        //执行A*算法，起点（0,0）,终点（8,7）,结果保存至堆栈
        Deque<MapNode> result = a.pathPlanning(new MapNode(0, 0), new MapNode(4,5));
        Deque<MapNode> result2 = a.pathPlanning(new MapNode(4, 5), new MapNode(8,7));
        Deque<MapNode> result3 = a.pathPlanning(new MapNode(8, 7), new MapNode(6,16));
        Deque<MapNode> result4 = a.pathPlanning(new MapNode(6, 16), new MapNode(15,10));
        Deque<MapNode> result5 = a.pathPlanning(new MapNode(15, 10), new MapNode(18,7));
        Deque<MapNode> result6 = a.pathPlanning(new MapNode(18, 7), new MapNode(16,16));
        result2.forEach(v->{
            result.add(v);
        });
        result3.forEach(v->{
            result.add(v);
        });
        result4.forEach(v->{
            result.add(v);
        });
        result5.forEach(v->{
            result.add(v);
        });
        result6.forEach(v->{
            result.add(v);
        });
        //打印结果
        a.printResult(result);
    }
}

