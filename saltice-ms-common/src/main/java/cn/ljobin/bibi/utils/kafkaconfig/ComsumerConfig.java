//package cn.ljobin.bibi.utils.kafkaconfig;
//
//import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import static cn.stylefeng.roses.kernel.model.constants.ConfigPrefixConstants.LOG_PREFIX;
//
///**
// * @program: IoT-Plat
// * @description: kafka消费者监听器
// * @author: Mr.Liu
// * @create: 2020-04-14 17:06
// **/
//@Configuration
//@ConditionalOnProperty(prefix = LOG_PREFIX, value = "kafka", havingValue = "true")
//public class ComsumerConfig {
//    @Bean
//    public Listener listener() {
//        return new Listener();
//    }
//}
