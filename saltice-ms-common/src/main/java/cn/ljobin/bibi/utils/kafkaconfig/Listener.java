//package cn.ljobin.bibi.utils.kafkaconfig;
//
//import lombok.extern.slf4j.Slf4j;
//import org.apache.kafka.clients.consumer.ConsumerRecord;
//import org.springframework.kafka.annotation.KafkaListener;
//import org.springframework.kafka.annotation.TopicPartition;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//
//import static cn.stylefeng.roses.kernel.logger.constants.KafkaConstants.*;
//import static cn.stylefeng.roses.kernel.model.constants.ConfigPrefixConstants.LOG_PREFIX;
//
///**
// * @program: IoT-Plat
// * @description:
// * @author: Mr.Liu
// * @create: 2020-04-14 11:07
// **/
//@Service
//@Slf4j
//public class Listener {
//    /**
//     * LOG_TOPIC 消息接收
//     * @param recordList
//     */
//    @KafkaListener(id=LOG_TOPIC,
//            topicPartitions ={@TopicPartition(topic = LOG_TOPIC, partitions = { "0"})})
//    public void listen(List<ConsumerRecord<String, byte[]>> recordList) {
//        recordList.forEach((record)->{
//            log.info("kafka的key: " + record.key());
//            log.info("kafka的value: " + new String(record.value()));
//        });
//    }
//
//    /**
//     * TRACE_LOG_TOPIC 消息接收
//     * @param recordList
//     */
//    @KafkaListener(id=TRACE_LOG_TOPIC,
//            topicPartitions ={@TopicPartition(topic = TRACE_LOG_TOPIC, partitions = { "0"})})
//    public void listen2(List<ConsumerRecord<String, byte[]>> recordList) {
//        recordList.forEach((record)->{
//            log.info("kafka的key: " + record.key());
//            log.info("kafka的value: " + new String(record.value()));
//        });
//    }
//
//    /**
//     * TC_LOG_TOPIC 消息接收
//     * @param recordList
//     */
//    @KafkaListener(id=TC_LOG_TOPIC,
//                topicPartitions ={@TopicPartition(topic = TC_LOG_TOPIC, partitions = { "0"})})
//        public void listen3(List<ConsumerRecord<String, byte[]>> recordList) {
//            recordList.forEach((record)->{
//                log.info("kafka的key: " + record.key());
//                log.info("kafka的value: " + new String(record.value()));
//            });
//    }
//}
