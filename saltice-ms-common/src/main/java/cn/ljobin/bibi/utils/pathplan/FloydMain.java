package cn.ljobin.bibi.utils.pathplan;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @program: guns
 * @description:
 * @author: Mr.Liu
 * @create: 2019-09-26 09:57
 **/
public class FloydMain {
    private static int max=Integer.MAX_VALUE;
    private static double [][]dist;
    private static int [][]path;
    private static ArrayList list=new ArrayList<Integer>();
    public static void findCheapestPath(int begin,int end,double Arcs[][]){
        floyd(Arcs);
        list.clear();
        list.add(begin);
        findPath(begin,end);
        list.add(end);
    }

    public static void findPath(int i,int j){
        int k=path[i][j];
        if(k==-1)
            return ;
        findPath(i,k);
        list.add(k);
        findPath(k,j);
    }

    public static void floyd(double [][] Arcs){
        int n=Arcs.length;
        for(int i=0;i<n;i++)
            for(int j=0;j<n;j++){
                //初始化当前的路径长度表
                path[i][j]=-1;
                //初始化当前的路径表
                dist[i][j]=Arcs[i][j];
            }

        for(int k=0;k<n;k++)
            for(int i=0;i<n;i++)
                for(int j=0;j<n;j++){
                    if(dist[i][k]!=max&&dist[k][j]!=max&&dist[i][k]+dist[k][j]<dist[i][j]){
                        dist[i][j]=dist[i][k]+dist[k][j];
                        path[i][j]=k;
                    }
                }
    }
    public static double Get(int N,int K,int[] X,int headx,int heady){
        boolean[] visited=new boolean[N+1];
        double [][]Arcs=new double[N+1][N+1];
        for(int i=0;i<N;i++){
            for(int j=0;j<N;j++){
                if(i==j){
                    Arcs[i][j]=0;
                }
                else{
                    Arcs[i][j]=Double.parseDouble(String.format("%.6f",(double)Math.abs(X[i]-X[j])));
                }
            }
        }
        for(int i=0;i<N;i++){
            double _x = Math.abs(headx - X[i]);
            double _y = Math.abs(heady - 0);
            double l=Double.parseDouble(String.format("%.6f",Math.sqrt(_x*_x+_y*_y)));
            Arcs[i][N]=l;
            Arcs[N][i]=l;
        }
        Arcs[N][N]=0;
        for(int i=0;i<N+1;i++){
            for(int j=0;j<N+1;j++){
                System.out.print(Arcs[i][j]+" ");
            }
            System.out.println();
        }
        //======================
        double min=0;
        int count=0;
        for(int i=0;i<N+1;i++){
            visited[i]=true;
            double mintemp=Double.MAX_VALUE;
            for(int j=0;j<N+1;j++){
                findCheapestPath(i, j, Arcs);
                ArrayList<Integer>L=list;
                System.out.println("路径长度:"+dist[i][j]);
                if(dist[i][j]<mintemp && !visited[j]){
                    mintemp=dist[i][j];
                }
            }
            System.out.println("长度:"+i+":  "+mintemp);
            min+=mintemp;
            count++;
            System.out.println("=============="+min);
            if(count==N){
                return min;
            }

        }
        return min;
    }
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        int[] X=new int[]{1,0,2};
        dist=new double[4][4];
        path=new int[4][4];
        double[][] path2  = new double[8][8];
        MapNode[] mapNodes = {
                new MapNode(2.0,4.0),new MapNode(3.0,2.0),
                new MapNode(1.0,5.0),new MapNode(2.0,1.0),
                new MapNode(4.0,5.0),new MapNode(3.0,7.0),
                new MapNode(2.0,2.0),new MapNode(5.0,4.0)
        };
        for (int i = 0; i < 8; i++) {
            double[] ddL = getAllLength(mapNodes, mapNodes[i]);
            int length = ddL.length;
            for (int j = 0; j <length ; j++) {
                path2[i][j] = ddL[j];
                System.out.print(ddL[j]+"--");
            }
            System.err.println("");;
        }
        String[] info = {"s1","s2","s3","s4","s5","s6","s7","s8"};
        Graph<String> graph = new Graph<String>(path2,info);

        FloydMain floydMain = new FloydMain();
        floydMain.racs = graph.racs2;
        floydMain.verticeNum = graph.verticeNum;
        floydMain.verticeInfo = graph.verticeInfo;
        floydMain.currDist = graph.currDist2;
        floydMain.fordAlgorithm(1);
        //System.out.println(Double.parseDouble(String.format("%.6f",Get(3,1,X,1,1))));
    }

    /**
     * 求每个点距其他点的距离
     * @param data
     * @return
     */
    public static double[] getAllLength(MapNode[] data, MapNode tag){

        int length  = data.length;
        double[] results = new double[length];
        for (int i=0;i<length;i++){
            BigDecimal b = new BigDecimal(distanceTwoPoint(tag,data[i]));
            results[i] = b.setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
            //results[i]=(double) Math.round(distanceTwoPoint(tag,data[i]) * 100) / 100;;
        }
        return results;
    }
    /**
     * 求每个点距其他点的最小距离，不包括自己
     * @param data
     * @return 最小的点,没有返回null
     */
    public static MapNode getMinLength(MapNode[] data, MapNode tag){
        int length  = data.length;
        double results = 0.0;
        MapNode mapNode = null;
        for (int i=0;i<length;i++){
            if(!data[i].equals(tag)){
                if(results>distanceTwoPoint(tag,data[i])){
                    results=distanceTwoPoint(tag,data[i]);
                    mapNode = data[i];
                }
            }
        }
        return mapNode;
    }
    /**
     * 计算二维数组空间两个点的距离
     * @param a 计算点
     * @param b 被计算点
     * @return
     */
    public static double distanceTwoPoint(MapNode a,MapNode b){
        double a_x = a.getX();
        double a_y = a.getY();
        double b_x = b.getX();
        double b_y = b.getY();
        return Math.sqrt(Math.abs((a_x - b_x)*(a_x - b_x))+Math.abs((a_y-b_y)*(a_y-b_y)));
    }
    /**
     * 使用Ford的方法寻找最短路径
     * @param first 路径开始的顶点
     */
    private int verticeNum ;
    private double[] currDist;
    private double[][] racs;
    private String[] verticeInfo;
    public double[] fordAlgorithm(int first){
        if(first < 0 || first >= verticeNum ){
            throw new IndexOutOfBoundsException ("should between 0 ~ " + (verticeNum -1));
        }
        setNumberAsInfinitie();
        currDist[first] = 0;
        while(true){
            //是否有使currDist更小的边
            boolean hasLessEdge = false;
            for(int s = 0 ; s < verticeNum; s ++){
                for (int e = 0; e < verticeNum; e ++){
                    if(racs[s][e] != Double.MAX_VALUE){
                        double weight = getWeightPreventOverflow(s,e);
                        if(currDist[e] > currDist[s] + weight){
                            hasLessEdge = true;
                            currDist[e] = currDist[s] + racs[s][e];
                        }
                    }
                }
            }
            if(!hasLessEdge) { break; }
        }
        for(int i = 0; i < currDist.length; i++){
            System.out.println("现在顶点 " + verticeInfo[i].toString() + " 距离顶点 " + verticeInfo[first].toString()  + " 的最短距离为 " + currDist[i]);
        }

        return currDist;
    }

    /**
     * 处理并获得权重,并且使得到的结果在进行路径长度的加减操作时不会出现溢出
     * @param start
     * @param end
     * @return
     */
    private double getWeightPreventOverflow(int start, int end){
        double weight = 0;
        //防止加减法溢出
        if(currDist[start] == Integer.MAX_VALUE && racs[start][end] > 0){
            weight = 0;
        }else if(currDist[start] == Integer.MIN_VALUE && racs[start][end] < 0){
            weight = 0;
        }else{
            weight = racs[start][end];
        }
        return weight;
    }
    /**
     * 使用 Dijkstra算法寻找最短路径
     * @param first 路径开始的顶点
     * @return 返回最后的最短路径
     */
    public double[] dijkstraAlgorithm(int first){
        if(first < 0 || first >= verticeNum ){
            throw new IndexOutOfBoundsException ("should between 0 ~ " + (verticeNum -1));
        }
        setNumberAsInfinitie();
        currDist[first] = 0;
        List<Integer> toBeChecked = new LinkedList<>();
        for(int i = 0; i < verticeNum; i ++){
            toBeChecked.add(i);
        }
        while(!toBeChecked.isEmpty()){
            int nowVertice = findMinCurrDistVerticeAndRemoveFormList(toBeChecked);
            for(int i = 0; i < verticeNum; i ++){
                int nextVertice = -1;                       //邻接节点
                double weight = Double.MAX_VALUE;             //到达邻接节点的权重
                if(racs[nowVertice][i] != Integer.MAX_VALUE){   //得到邻接顶点
                    if(toBeChecked.contains(i)){
                        nextVertice = i;
                        weight = racs[nowVertice][i];
                    }
                }
                if(nextVertice == -1) {continue;}
                if(currDist[nextVertice] > currDist[nowVertice] + weight){
                    currDist[nextVertice] = currDist[nowVertice] + weight;
                }
            }

        }
        for(int i = 0; i < currDist.length; i++){
            System.out.println("现在顶点 " + verticeInfo[i].toString() + " 距离顶点 " + verticeInfo[first].toString()  + " 的最短距离为 " + currDist[i]);
        }
        return currDist;
    }
  /**
     * 将currDist数组初始化为无穷大
     */
    private void setNumberAsInfinitie(){
        currDist = new double[verticeNum];
        for (int i = 0; i < verticeNum; i++){
            currDist[i] = Integer.MAX_VALUE;
        }
    }

    /**
     * 寻找出当前距离起始顶点路径最短的顶点,并将其从toBeCheck中删除
     * @param list
     * @return
     */
    private int findMinCurrDistVerticeAndRemoveFormList(List<Integer> list){
        int num = list.get(0);
        double dist = currDist[list.get(0)];
        int listIndex = 0;
        for(int i = 1; i < list.size(); i ++){
            int index = list.get(i);
            if(currDist[index] < dist) {
                dist = currDist[index];
                num = index;
                listIndex = i;
            }
        }
        list.remove(listIndex);
        return num;
    }
    /**
     * 使用邻接矩阵实现图<p>
     * 深度优先遍历与广度优先遍历<p>
     * 求最短路径:<p>
     *      1. Dijkstra 算法 <p>
     *      2. Ford 算法 <p>
     *      3. 通用型的纠正标记算法<p>
     * Created by Henvealf on 16-5-22.
     */
    public static class Graph<T> {
        private int[][] racs;       //邻接矩阵
        private double[][] racs2;       //邻接矩阵 2
        private T[] verticeInfo;   //各个点所携带的信息.

        private int verticeNum;             //顶点的数目,
        private int[] visitedCount;         //记录访问
        private int[] currDist;             //最短路径算法中用来记录每个顶点距离起始顶点路径的长度.
        private double[] visitedCount2;         //记录访问 2
        private double[] currDist2;             //最短路径算法中用来记录每个顶点距离起始顶点路径的长度. 2

        public Graph(int[][] racs, T[] verticeInfo){
            if(racs.length != racs[0].length){
                throw new IllegalArgumentException("racs is not a adjacency matrix!");
            }
            if(racs.length != verticeInfo.length ){
                throw new IllegalArgumentException ("Argument of 2 verticeInfo's length is error!");
            }
            this.racs = racs;
            this.verticeInfo = verticeInfo;
            verticeNum = racs.length;
            visitedCount = new int[verticeNum];
        }
        public Graph(double[][] racs2, T[] verticeInfo){
            if(racs2.length != racs2[0].length){
                throw new IllegalArgumentException("racs is not a adjacency matrix!");
            }
            if(racs2.length != verticeInfo.length ){
                throw new IllegalArgumentException ("Argument of 2 verticeInfo's length is error!");
            }
            this.racs2 = racs2;
            this.verticeInfo = verticeInfo;
            verticeNum = racs2.length;
            visitedCount2 = new double[verticeNum];
        }
        //..........
    }


}
