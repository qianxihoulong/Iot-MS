package cn.ljobin.bibi.utils;

import cn.ljobin.bibi.utils.cache.Cache;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;


/**
 * @program: guns
 * @description: 读取excel数据
 * @author: Mr.Liu
 * @create: 2019-09-17 21:03
 **/

public class ReadExcel {
    private Logger logger = LoggerFactory.getLogger(ReadExcel.class);
    private Workbook wb;
    private Sheet sheet;
    private Row row;
    public static final String EXCEL_CACHE = "excelData";

    public ReadExcel() {
    }

    public ReadExcel(String filepath) {
        if(filepath==null){
            return;
        }
        String ext = filepath.substring(filepath.lastIndexOf("."));
        try {
            InputStream is = new FileInputStream(filepath);
            if(".xls".equals(ext)){
                wb = new HSSFWorkbook(is);
            }else if(".xlsx".equals(ext)){
                wb = new XSSFWorkbook(is);
            }else{
                wb=null;
            }
        } catch (FileNotFoundException e) {
            logger.error("FileNotFoundException", e);
        } catch (IOException e) {
            logger.error("IOException", e);
        }
    }

    /**
     * 读取Excel表格表头的内容
     *
     * @return String 表头内容的数组
     * @author zengwendong
     */
    public String[] readExcelTitle() throws Exception{
        if(wb==null){
            throw new Exception("Workbook对象为空！");
        }
        sheet = wb.getSheetAt(0);
        row = sheet.getRow(0);
        // 标题总列数
        int colNum = row.getPhysicalNumberOfCells();
        System.out.println("colNum:" + colNum);
        String[] title = new String[colNum];
        for (int i = 0; i < colNum; i++) {
            // title[i] = getStringCellValue(row.getCell((short) i));
            title[i] = row.getCell(i).getCellFormula();
        }
        return title;
    }

    /**
     * 读取Excel数据内容
     *
     * @return Map 包含单元格数据内容的Map对象
     * @author zengwendong
     */
    public Map<Integer, Map<Integer,Object>> readExcelContent() throws Exception{
        if(wb==null){
            throw new Exception("Workbook对象为空！");
        }
        Map<Integer, Map<Integer,Object>> content = new HashMap<Integer, Map<Integer,Object>>();

        sheet = wb.getSheetAt(0);
        // 得到总行数
        int rowNum = sheet.getLastRowNum();
        System.out.println("总行数:"+rowNum);
        row = sheet.getRow(0);
        int colNum = row.getPhysicalNumberOfCells();
        System.out.println("总列数:"+colNum);
        // 正文内容应该从第二行开始,第一行为表头的标题
        for (int i = 1; i <= rowNum; i++) {
            row = sheet.getRow(i);
            int j = 0;
            Map<Integer,Object> cellValue = new HashMap<Integer, Object>();
            while (j < colNum) {
                Object obj = getCellFormatValue(row.getCell(j));
                cellValue.put(j, obj);
                j++;
            }
            content.put(i, cellValue);
        }
        return content;
    }

    /**
     *
     * 根据Cell类型设置数据
     *
     * @param cell
     * @return
     * @author zengwendong
     */
    private Object getCellFormatValue(Cell cell) {
        Object cellvalue = "";
        if (cell != null) {
            // 判断当前Cell的Type
            switch (cell.getCellType()) {
                case Cell.CELL_TYPE_NUMERIC:// 如果当前Cell的Type为NUMERIC
                case Cell.CELL_TYPE_FORMULA: {
                    // 判断当前的cell是否为Date
                    if (DateUtil.isCellDateFormatted(cell)) {
                        // 如果是Date类型则，转化为Data格式
                        // data格式是带时分秒的：2013-7-10 0:00:00
                        // cellvalue = cell.getDateCellValue().toLocaleString();
                        // data格式是不带带时分秒的：2013-7-10
                        Date date = cell.getDateCellValue();
                        cellvalue = date;
                    } else {// 如果是纯数字

                        // 取得当前Cell的数值
                        cellvalue = String.valueOf(cell.getNumericCellValue());
                    }
                    break;
                }
                case Cell.CELL_TYPE_STRING:// 如果当前Cell的Type为STRING
                    // 取得当前的Cell字符串
                    cellvalue = cell.getRichStringCellValue().getString();
                    break;
                default:// 默认的Cell值
                    cellvalue = "";
            }
        } else {
            cellvalue = "";
        }
        return cellvalue;
    }

    public static void main(String[] args) {
        try {
            String filepath = "C:\\Users\\liuyanbin\\Desktop\\中药柜药名(1).xlsx";
            InputStream in = new FileInputStream(new File(filepath));
            ReadExcel excels = new ReadExcel();
            System.out.println(excels.importExcel(in,"中药柜药名(1).xlsx"));
//            ReadExcel excelReader = new ReadExcel(filepath);
//            // 对读取Excel表格标题测试
////			String[] title = excelReader.readExcelTitle();
////			System.out.println("获得Excel表格的标题:");
////			for (String s : title) {
////				System.out.print(s + " ");
////			}
//
//            // 对读取Excel表格内容测试
//            Map<Integer, Map<Integer,Object>> map = excelReader.readExcelContent();
//            System.out.println("获得Excel表格的内容:");
//            List<MAddress> mAddressList = new ArrayList<>();
//            for (int i = 1; i <= map.size(); i++) {
//
//                List<String> list = new ArrayList<>(3);
//                map.get(i).values().forEach(v->{
//                    System.out.println(((String) v));
//                    list.add( v==null || "".equals((String)v)?"0":(String) v);
//                });
//               // System.out.println(list.toString());
////                if(list.size()!=3){
////                    System.err.println("列数不对");
////                    return;
////                }
////                MAddress mAddress = new MAddress(list.get(0),list.get(1),1,Integer.parseInt(list.get(2)));
////                mAddressList.add(mAddress);
//            }
//            mAddressList.forEach(System.out::println);
        } catch (FileNotFoundException e) {
            System.out.println("未找到指定路径的文件!");
            e.printStackTrace();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
    public  String importExcel(InputStream inputStream, String fileName) throws Exception{

        String message = "200";

        boolean isE2007 = false;
        //判断是否是excel2007格式
        if(fileName.endsWith("xlsx")){
            isE2007 = true;
        }

        int rowIndex = 0;
        try {
            InputStream input = inputStream;  //建立输入流
            Workbook wb;
            //根据文件格式(2003或者2007)来初始化
            if(isE2007){
                wb = new XSSFWorkbook(input);
            }else{
                wb = new HSSFWorkbook(input);
            }
            //获得第一个表单
            Sheet sheet = wb.getSheetAt(0);
            int rowCount = sheet.getLastRowNum()+1;
            //保存所有个中药柜子中的数据
            List<List<String>> lists = new ArrayList<>();
            //保存每个中药柜子中的数据
            Map<String,List<List<String>>> map = new HashMap<>();
            String boxName = "中药柜";
            System.err.println(rowCount);
            for(int i = 0; i < rowCount;i++){
                List<String> stringList = new ArrayList<>();
                rowIndex = i;
                Row row ;
                //默认读取一行中100列的数据
                for(int j = 0;j<100;j++){
                    //如果是合并单元格进入if中
                    if(isMergedRegion(sheet,i,j)){
                        if(getMergedRegionValue(sheet,i,j) != null){
                            stringList.add(getMergedRegionValue(sheet,i,j));
                        }
                    }else{
                        row = sheet.getRow(i);
                        if(row.getCell(j)!=null){
                            stringList.add(row.getCell(j).toString());
                        }
                    }
                }
                lists.add(stringList);
            }
            List<String> BoxList = new ArrayList<>();
            String fg = " & ";
            for(List<String> v:lists){
                if(v.toString().contains(boxName)){
                    int size = v.size();
                    StringBuilder stringBuilder = new StringBuilder("");
                    for (int i = 0;i<size;i++){
                        if(v.get(i)!=null & !"".equals(v.get(i))){
                            if(!stringBuilder.toString().contains(v.get(i))){
                                stringBuilder.append(v.get(i)).append(fg);
                            }
                        }
                    }
                    //删除末尾的逗号
                    BoxList.add(stringBuilder.toString().substring(0,stringBuilder.toString().length()-fg.length()));
                }
            }
            System.err.println(BoxList.toString());
            int tag = -1;
            int tag2 = 0;
            Map<String,List<String>> maps = new HashMap<>();
                //获取每个药柜中的数目和分开保存
                for (List<String> v:lists){
                    if(v.toString().contains(boxName)){
                        if(tag>=0){
                            List<String> ls = new ArrayList<>();
                            ls.add(String.valueOf(tag2));
                            maps.put(BoxList.get(tag),ls);
                        }
                        tag++;
                        tag2=0;
                    }else {
                        maps.put(BoxList.get(tag)+tag2,v);
                        tag2++;
                    }
                }
                if(tag>=0){
                    List<String> ls = new ArrayList<>();
                    ls.add(String.valueOf(tag2));
                    maps.put(BoxList.get(tag),ls);
                }
            for (int j =0;j<BoxList.size();j++){
                List<List<String>> listList = new ArrayList<>();
                int ll = Integer.parseInt(maps.get(BoxList.get(j)).get(0));
                for (int i=0;i<ll;i++){
                    listList.add(maps.get(BoxList.get(j)+i));
                }
                map.put(BoxList.get(j),listList);
            }
            ConcurrentHashMap<String,String> concurrentMap = new ConcurrentHashMap<>();
            map.forEach((k,v)->{
                //System.out.println(k+"--->>>"+v.toString()+"--->>>"+v.size());
                int len = v.size();
                for (int i=0;i<len;i++){
                    int size = v.get(i).size();
                    for (int j=0;j<size;j++){
                        //System.err.println(v.get(i).get(j));
                        if("".equals(v.get(i).get(j))){
                            System.out.println("null");
                        }else {
                            //System.out.println(v.get(i).get(j));
                            //将每一个柜子中的药按中文逗号分开
                            String[] strings = v.get(i).get(j).split("，");
                            //System.err.println(Arrays.toString(strings));
                            if(strings.length==0){
                                continue;
                            }
                            int index = 0;
                            for (String item:strings) {
                                if (concurrentMap.containsKey(item)){
                                    int in = getIndex(concurrentMap,item,1);
                                    if(in>1){
                                        concurrentMap.put(item+"("+in+")",k+"-->第"+(i+1)+"行，第"+(j+1)+"列"+"-->格内号:"+(++index));
                                    }else {
                                        concurrentMap.put(item,k+"-->第"+(i+1)+"行，第"+(j+1)+"列"+"-->格内号:"+(++index));
                                    }
                                }else {
                                    concurrentMap.put(item,k+"-->第"+(i+1)+"行，第"+(j+1)+"列"+"-->格内号:"+(++index));
                                }
                            }
                        }
                    }
                }
            });
            concurrentMap.forEach((k,v)->{
                  System.out.println(k+"--->>>>"+v);
            });
            System.err.println(concurrentMap.size());
            Cache.put(EXCEL_CACHE,concurrentMap);
        } catch (Exception ex) {
            ex.printStackTrace();
            message =  200+rowIndex+"";
        }
        return message;
    }

    /**
     *
     * @param concurrentHashMap
     * @param key
     * @param index  1
     * @return
     */
    public static int getIndex(ConcurrentHashMap<String,String> concurrentHashMap,String key,int index){
        if(concurrentHashMap.containsKey(key)){
            return getIndex2(concurrentHashMap, key,key+"("+(index+1)+")",index+1);
        }
        return index;
    }
    public static int getIndex2(ConcurrentHashMap<String,String> concurrentHashMap,String oldKey,String newKey,int index){
        if(concurrentHashMap.containsKey(newKey)){
            return getIndex2(concurrentHashMap, oldKey,oldKey+"("+(index+1)+")",index+1);
        }
        return index;
    }
    /**
     * 获取单元格的值
     * @param cell
     * @return
     */
    public  String getCellValue(Cell cell){
        if(cell == null) {
            return "";
        }
        return cell.getStringCellValue();
    }


    /**
     * 合并单元格处理,获取合并行
     * @param sheet
     * @return List<CellRangeAddress>
     */
    public  List<CellRangeAddress> getCombineCell(Sheet sheet)
    {
        List<CellRangeAddress> list = new ArrayList<>();
        //获得一个 sheet 中合并单元格的数量
        int sheetmergerCount = sheet.getNumMergedRegions();
        //遍历所有的合并单元格
        for(int i = 0; i<sheetmergerCount;i++)
        {
            //获得合并单元格保存进list中
            CellRangeAddress ca = sheet.getMergedRegion(i);
            list.add(ca);
        }
        return list;
    }

    private  int getRowNum(List<CellRangeAddress> listCombineCell,Cell cell,Sheet sheet){
        int xr = 0;
        int firstC = 0;
        int lastC = 0;
        int firstR = 0;
        int lastR = 0;
        for(CellRangeAddress ca:listCombineCell)
        {
            //获得合并单元格的起始行, 结束行, 起始列, 结束列
            firstC = ca.getFirstColumn();
            lastC = ca.getLastColumn();
            firstR = ca.getFirstRow();
            lastR = ca.getLastRow();
            if(cell.getRowIndex() >= firstR && cell.getRowIndex() <= lastR)
            {
                if(cell.getColumnIndex() >= firstC && cell.getColumnIndex() <= lastC)
                {
                    xr = lastR;
                }
            }

        }
        return xr;

    }
    /**
     * 判断单元格是否为合并单元格，是的话则将单元格的值返回
     * @param listCombineCell 存放合并单元格的list
     * @param cell 需要判断的单元格
     * @param sheet sheet
     * @return
     */
    public  String isCombineCell(List<CellRangeAddress> listCombineCell,Cell cell,Sheet sheet)
            throws Exception{
        int firstC = 0;
        int lastC = 0;
        int firstR = 0;
        int lastR = 0;
        String cellValue = null;
        for(CellRangeAddress ca:listCombineCell)
        {
            //获得合并单元格的起始行, 结束行, 起始列, 结束列
            firstC = ca.getFirstColumn();
            lastC = ca.getLastColumn();
            firstR = ca.getFirstRow();
            lastR = ca.getLastRow();
            if(cell.getRowIndex() >= firstR && cell.getRowIndex() <= lastR)
            {
                if(cell.getColumnIndex() >= firstC && cell.getColumnIndex() <= lastC)
                {
                    Row fRow = sheet.getRow(firstR);
                    Cell fCell = fRow.getCell(firstC);
                    cellValue = getCellValue(fCell);
                    break;
                }
            }
            else
            {
                cellValue = "";
            }
        }
        return cellValue;
    }

    /**
     * 获取合并单元格的值
     * @param sheet
     * @param row
     * @param column
     * @return
     */
    public  String getMergedRegionValue(Sheet sheet ,int row , int column){
        int sheetMergeCount = sheet.getNumMergedRegions();

        for(int i = 0 ; i < sheetMergeCount ; i++){
            CellRangeAddress ca = sheet.getMergedRegion(i);
            int firstColumn = ca.getFirstColumn();
            int lastColumn = ca.getLastColumn();
            int firstRow = ca.getFirstRow();
            int lastRow = ca.getLastRow();

            if(row >= firstRow && row <= lastRow){
                if(column >= firstColumn && column <= lastColumn){
                    Row fRow = sheet.getRow(firstRow);
                    Cell fCell = fRow.getCell(firstColumn);
                    return getCellValue(fCell) ;
                }
            }
        }

        return null ;
    }


    /**
     * 判断指定的单元格是否是合并单元格
     * @param sheet
     * @param row 行下标
     * @param column 列下标
     * @return
     */
    private  boolean isMergedRegion(Sheet sheet,int row ,int column) {
        int sheetMergeCount = sheet.getNumMergedRegions();
        for (int i = 0; i < sheetMergeCount; i++) {
            CellRangeAddress range = sheet.getMergedRegion(i);
            int firstColumn = range.getFirstColumn();
            int lastColumn = range.getLastColumn();
            int firstRow = range.getFirstRow();
            int lastRow = range.getLastRow();
            if(row >= firstRow && row <= lastRow){
                if(column >= firstColumn && column <= lastColumn){
                    return true;
                }
            }
        }
        return false;
    }
}
