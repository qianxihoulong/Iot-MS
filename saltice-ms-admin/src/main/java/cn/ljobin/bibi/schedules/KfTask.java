package cn.ljobin.bibi.schedules;

import java.util.Objects;

/**
 * @program: guns
 * @description: 开方中药路径规划任务类
 * @author: Mr.Liu
 * @create: 2019-09-30 19:55
 **/
public class KfTask {
    /**
     * 开方 id
     */
    private String kfId;
    /**
     * 紧急标志位 1代表有效，优先处理 ， 0 代表正常
     */
    private int tag;

    public KfTask(String kfId, int tag) {
        this.kfId = kfId;
        this.tag = tag;
    }

    public int getTag() {
        return tag;
    }

    public void setTag(int tag) {
        this.tag = tag;
    }

    public String getKfId() {
        return kfId;
    }

    public void setKfId(String kfId) {
        this.kfId = kfId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        KfTask kfTask = (KfTask) o;
        return tag == kfTask.tag &&
                Objects.equals(kfId, kfTask.kfId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(kfId, tag);
    }
}
