package cn.ljobin.bibi;

import cn.ljobin.bibi.roses.utils.SpringContextUtils;
import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @program: nettyTCP
 * @description:
 * @author: Mr.Liu
 * @create: 2019-08-02 17:56
 **/
/**如果有其它的包需要被扫描，就在下面添加**/
/**cn.stylefeng.roses.kernel.logger.api是日志异步操作的**/
@SpringBootApplication(scanBasePackages = {"cn.ljobin.bibi","cn.stylefeng.roses"},exclude = {DruidDataSourceAutoConfigure.class, DataSourceAutoConfiguration.class})
/**开启定时器任务**/
@EnableScheduling
@EnableFeignClients
@EnableDiscoveryClient
@MapperScan("cn.ljobin.bibi.mapper")
public class BibiApplication {
    private final static Logger logger = LoggerFactory.getLogger(BibiApplication.class);
    public static ConfigurableApplicationContext applicationContext;
    public static void main(String[] args) {
        applicationContext =  SpringApplication.run(BibiApplication.class, args);
        SpringContextUtils.setApplicationCtx(applicationContext);
        logger.info("系统启动成功");
    }

}
