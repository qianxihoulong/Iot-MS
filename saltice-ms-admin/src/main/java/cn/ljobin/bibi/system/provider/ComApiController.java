package cn.ljobin.bibi.system.provider;


import cn.ljobin.bibi.api.ComService;
import cn.ljobin.bibi.config.netty.NettyConfig;
import cn.ljobin.bibi.feign.AuthUserComsumer;
import cn.ljobin.bibi.model.Com;
import cn.ljobin.bibi.model.MStream;
import cn.ljobin.bibi.netty.client.Slave;
import cn.ljobin.bibi.netty.client.Voice;
import cn.ljobin.bibi.netty.handler.RxtxHandler;
import cn.ljobin.bibi.netty.util.ByteUtil;
import cn.ljobin.bibi.netty.util.NettyCom;
import cn.ljobin.bibi.netty.voice.VoiceWriteAndFlush;
import cn.ljobin.bibi.utils.cache.Cache;
import cn.ljobin.bibi.utils.httpclient.HttpClientUtil;
import cn.stylefeng.roses.core.reqres.response.ResponseData;
import cn.stylefeng.roses.core.util.HttpContext;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import io.netty.channel.Channel;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.ParseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @program: guns
 * @description: 串口控制
 * @author: Mr.Liu
 * @create: 2019-10-07 13:53
 **/
@RequestMapping("/com")
@Controller
public class ComApiController {
    /**用来判断一个字符串能否转化为int**/
    public static final Pattern PATTERN = Pattern.compile("0|([-]?[1-9][0-9]*)");
    public static final Pattern INT = Pattern.compile("\\d+");
    public static boolean isInt(String str) {
        try {
            Integer.valueOf(str);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    /**用来保存取药的 <uid:list<String>>
     * 在发送数据的时候才进行转换为【药柜号（1个字节）+行号（1个字节）+列号（1个字节）+格内号（1个字节）+药剂量（两个字节）】
     * 按十六进制保持 如 01 03 03 03 00 32
     **/
    public static final ConcurrentMap<String,List<String>> KFMAP = RxtxHandler.KFMAP;
    /**保存当前登陆账号**/
    public static  String uidOnLine = "client";
    private static final Logger logger = LoggerFactory.getLogger(ComApiController.class);
    private String PREFIX = "/system/com/";

    @Autowired
    private ComService comService;
    @Autowired
    private AuthUserComsumer authUserComsumer;

    private Long getUidFromFeign(HttpServletRequest request){
        String token = HttpContext.getTokenFromRequest(request);
        return authUserComsumer.getLoginUserIdByToken(token);
    }

    @GetMapping("/androidL")
    public String androidL(){
        return "/system/kf/kf_android.html";
    }

    @RequestMapping("/")
    public String index(){
        return PREFIX+"index.html";
    }
    @RequestMapping("/kf/")
    public String kf(HttpServletResponse response){
        return "redirect:http://47.103.1.210:8082/ENG/Madic/homepage.html";
        //return "/system/Madic/homepage.html";
    }
    @GetMapping("/getOnLineCom")
    @ResponseBody
    public List<Com> getOnLineCom(){
        return comService.selectAllCom(1);
    }
    @GetMapping("/getUid")
    @ResponseBody
    public ResponseData getUid(HttpServletRequest request){
        Long id = getUidFromFeign(request);
        return ResponseData.success(id != null ? id : -1);
    }

    @GetMapping("/upUid/{uid}")
    @ResponseBody
    public ResponseData upUid(@PathVariable("uid") String uid) throws IOException {
        if(uid==null|| "".equals(uid)){
            return ResponseData.error(401,"未获取到账号id");
        }
        //更新当前在线的账号到本地电脑
        uidOnLine = uid;
        JSONObject resultJsonObject=null;
        BufferedReader bufferedReader=null;
        // 获得Http客户端(可以理解为:你得先有一个浏览器;注意:实际上HttpClient与浏览器是不一样的)
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        // 创建Get请求 47.103.1.210:8082/guns-1.0.0
        HttpGet httpGet = new HttpGet("http://47.103.1.210:8082/guns-1.0.0/h5/kf_yao_getByid_loc/"+uid);
        // 响应模型
        CloseableHttpResponse response = null;
        //响应实体
        HttpEntity responseEntity;
        try {
            // 将上面的配置信息 运用到这个Get请求里
            httpGet.setConfig(HttpClientUtil.initRequestConfig());
            StringBuilder entityStringBuilder=new StringBuilder();
            // 由客户端执行(发送)Get请求
            response = httpClient.execute(httpGet);
            //得到httpResponse的状态响应码
            int statusCode= response.getStatusLine().getStatusCode();
            if (statusCode== HttpStatus.SC_OK) {
                //得到httpResponse的实体数据
                HttpEntity httpEntity=response.getEntity();
                if (httpEntity!=null) {
                    try {
                        bufferedReader=new BufferedReader
                                (new InputStreamReader(httpEntity.getContent(), StandardCharsets.UTF_8), 8*1024);
                        String line=null;
                        while ((line=bufferedReader.readLine())!=null) {
                            entityStringBuilder.append(line);
                        }
                        //利用从HttpEntity中得到的String生成JsonObject
                        resultJsonObject=JSONObject.parseObject(entityStringBuilder.toString());
                        if(!"200".equals(resultJsonObject.getString("code"))){
                            //如果服务器当前没有相应的要取药的药方 返回状态码：400，进入下面的逻辑
                            if("400".equals(resultJsonObject.getString("code"))){
                                return ResponseData.success(resultJsonObject.getString("message"));
                            }
                            return ResponseData.success(
                                    Integer.parseInt(resultJsonObject.getString("code")),
                                    resultJsonObject.getString("message"),
                                    resultJsonObject.getString("data"));
                        }
                        JSONArray ents = resultJsonObject.getJSONArray("data");
                        List<MStream> listThickness = getResultForArray(ents.toJSONString(), MStream.class);
                        //保存药品数据的集合
                        List<String> kfCode = new ArrayList<>();
                        int kfsLength=listThickness.size();
                        for (int i = 0; i < kfsLength; i++) {
                            StringBuilder kfBuilder = new StringBuilder("");
                            String box = listThickness.get(i).getTips().split("-->")[0];
                            String box_n = box.substring(box.length()-1);
                            String x = listThickness.get(i).getTips().split("-->")[1].split("，")[0];
                            String x_x = x.substring(1,x.length()-1);;
                            String y = listThickness.get(i).getTips().split("-->")[1].split("，")[1];
                            String y_y = y.substring(1,y.length()-1);
                            String geNei = intToHex(1);
                            String dose = "";
                            if(isInt(listThickness.get(i).getDose())){
                                int data = Integer.parseInt(listThickness.get(i).getDose());
                                if(data>65536){
                                    data = 0;
                                }
                                if(data/256>1){
                                    dose = intToHex(data/256)+intToHex(data%256);
                                }else {
                                    dose= "00"+intToHex(data);
                                }
                            }else {
                                Matcher m = INT.matcher(listThickness.get(i).getDose());
                                m.find();
                                int data = Integer.parseInt(m.group());
                                if(data>65536){
                                    data = 0;
                                }
                                if(data/256>1){
                                    dose = intToHex(data/256)+intToHex(data%256);
                                }else {
                                    dose= "00"+intToHex(data);
                                }
                            }
                            kfBuilder.append(intToHex(Integer.parseInt(box_n)))
                                    .append(intToHex(Integer.parseInt(x_x)))
                                    .append(intToHex(Integer.parseInt(y_y)))
                                    .append(geNei)
                                    .append(dose);
                            kfCode.add(kfBuilder.toString());
                        }
                        KFMAP.put(resultJsonObject.getString("message"),kfCode);
                        return ResponseData.success(
                                Integer.parseInt(resultJsonObject.getString("code")),
                                resultJsonObject.getString("message"),
                                listThickness);
                    } catch (Exception e) {
                        e.printStackTrace();
                        logger.error("HTTPCLIENT 获取取药数据失败：{}",e.getMessage());
                    }
                }
                logger.error("HTTPCLIENT 响应失败");
                return ResponseData.error(401,"响应失败");
            }
            logger.error("HTTPCLIENT 响应失败");
            return ResponseData.error(400,"请稍等一下重试");
        } catch (ParseException | IOException e) {
            e.printStackTrace();
            return ResponseData.error(400,"请重新启动程序");
        } finally {
            try {
                // 释放资源
                if (httpClient != null) {
                    httpClient.close();
                }
                if (response != null) {
                    response.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 将数字转为2位的16机制字符串
     * @param n
     * @return
     */
    public static String intToHex(int n) {
        return ByteUtil.numToLengthHexString(n,2).toUpperCase();
    }

    public static <T> List<T> getResultForArray(String result, Class<T> clazz) {
        List<T> re = null;
        try {
            if (result != null && result.length() > 0) {
                re = JSONArray.parseArray(result, clazz);
            }
        } catch (Exception e) {
            System.err.println("处理jsonArray失败");
        }
        return re;
    }
    @PostMapping("/runCom")
    @ResponseBody
    public int runCom(@RequestBody Map<String, String> com){
        int num = 0;
        int id = Integer.parseInt(com.get("DID"));
        String com2 = com.get("com")==null?"":com.get("com");
        if("".equals(com2) || !"COM".equals(com2.substring(0,3))){
            return -1;
        }
        String baudrate =  com.get("baudrate")==null?"115200":com.get("baudrate");
        String controlTime =  com.get("controlTime")==null?"":com.get("controlTime");
        String salt = com.get("salt")==null?"":com.get("salt");
        String status =  com.get("status")==null?"1":com.get("status");
        Com com1 = new Com(id,com2,baudrate,status.charAt(0),controlTime);

        //保存后即开始运行
        try {
            //语音模块使用
            if (id==3){
                NettyConfig.comPool.submit(new Runnable() {
                    @Override
                    public void run() {
                        Thread.currentThread().setName(com2);
                        Voice voice = new NettyCom().starVoiceCom(com2,Integer.parseInt(baudrate),uidOnLine);

                    }
                });
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //判断线程是否正在执行
                Voice voice = (Voice) Cache.get(com2);
                if(voice == null){
                    num = -1;
                    Cache.remove(com2);
                }else{
                    Channel channel = voice.getChannel();
                    if(channel != null && channel.isWritable()){
                        num = comService.updateCom(com1,1);
                        VoiceWriteAndFlush.voiceWriteAndFlush(channel,"语音功能开启成功");
                    }else {
                        num = -1;
                        voice.stop();
                        Cache.remove(com2);
                        System.err.println("失败");
                    }
                }

            }else {
                NettyConfig.comPool.submit(new Runnable() {
                    @Override
                    public void run() {
                        Thread.currentThread().setName(com2);
                        try {
                            NettyCom nettyCom = new NettyCom();
                            Slave slave = nettyCom.StarNewCom(com2, Integer.parseInt(baudrate),uidOnLine);
                            Channel channel = (Channel)Cache.get("channel");
                            if(channel == null){
                                Cache.remove(com2);
                                slave.stop();
                                logger.info("Com Run 【close】, COM ：{}，bt：{}",com2,baudrate);
                            }else {
                                //启动成功向netty服务器注册一下
                                RxtxHandler.writeAndFlushNettyAndUid(channel,uidOnLine);
                                logger.info("Com Run 【success】, COM ：{}，bt：{}",com2,baudrate);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            Cache.remove(com2);
                        }
                        //要是执行失败则会删除缓存中的com2
                    }
                });
                Thread.sleep(2000);
                //判断线程是否正在执行
                if(Cache.get(com2)!=null){
                    num = comService.updateCom(com1,1);
                }else {
                    num = -1;
                }
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
            logger.info("Com Run Exception：");
        }
        return num;
    }
    @PostMapping("/stopCom")
    @ResponseBody
    public int stopCom(@RequestBody Map<String, String> com){
        int num = 0;
        try {
            int id = Integer.parseInt(com.get("DID"));
            String com2 = com.get("com")==null?"":com.get("com");
            if("".equals(com2) || !"COM".equals(com2.substring(0,3))){
                return -1;
            }
            String baudrate =  com.get("baudrate")==null?"0":com.get("baudrate");
            if(Integer.parseInt(baudrate)<=0){
                return -2;
            }
            String controlTime =  com.get("controlTime")==null?"":com.get("controlTime");
            String salt = com.get("salt")==null?"":com.get("salt");
            String status =  com.get("status")==null?"1":com.get("status");
            Com com1 = new Com(id,com2,baudrate,status.charAt(0),controlTime);

            //保存后即开始运行
            try {
                if(id == 3){
                    Voice voice = (Voice) Cache.get(com2);
                    if (voice != null) {
                        voice.stop();
                    }
                }else {
                    Slave slave = (Slave) Cache.get(com2);
                    if (slave != null) {
                        slave.stop();
                    }
                }
                //判断线程是否正在执行
                if(Cache.get(com2)!=null){
                    Cache.remove(com2);
                    num = comService.updateCom(com1,1);
                }else {
                    comService.updateCom(com1,1);
                    num = -1;
                }
                logger.info("Com Stop success, COM：{}，bt：{}",com2,baudrate);
            }catch (Exception e){
                System.out.println(e.getMessage());
                logger.info("Com Run Exception：");
            }
        }catch (Exception e){
            e.printStackTrace();
            logger.info("Com Run Exception2：");
        }

        return num;
    }
    @PostMapping("/addCom")
    @ResponseBody
    public int addCom(@RequestBody Map<String, String> com){
        int num = 0;
        try {
            String com2 = com.get("com").trim();
            String baudrate =  com.get("baudrate").trim();
            String controlTime =  com.get("controlTime").trim();
            String salt = com.get("salt").trim();
            String status =  com.get("status").trim();
            String tips =  com.get("tips").trim();
            Com com1 = new Com(com2,tips,baudrate,status.charAt(0),controlTime);

            //保存后即开始运行
            try {
                logger.info("Comm启动成功,{}",com1);
                NettyConfig.comPool.submit(new Runnable() {
                    @Override
                    public void run() {
                        Thread.currentThread().setName(com2);
                        Cache.put(com2,Thread.currentThread().getName());
                        try {
                            NettyCom nettyCom = new NettyCom();
                            nettyCom.StarNewCom(com2, Integer.parseInt(baudrate),uidOnLine);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        //要是执行失败则会删除缓存中的com2
                        Cache.remove(com2);
                    }
                });
                //判断线程是否正在执行
                if(Cache.get(com2)!=null){
                    num = comService.insertCom(com1);
                }else {
                    num = -1;
                }
            }catch (Exception e){
                System.out.println(e.getMessage());
                logger.info("Comm运行异常：");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return num;
    }
    @PostMapping("/editCom")
    @ResponseBody
    public int editCom(@RequestBody Map<String, String> com){
        int num = 0;
        try {
            int id = Integer.parseInt(com.get("DID"));
            String com2 = com.get("com")==null?"":com.get("com");
            String baudrate =  com.get("baudrate")==null?"":com.get("baudrate");
            String controlTime =  com.get("controlTime")==null?"":com.get("controlTime");
            String tips =  com.get("tips")==null?"":com.get("tips");
            String salt = com.get("salt")==null?"":com.get("salt");
            String status =  com.get("status")==null?"1":com.get("status");
            Com com1 = new Com(id,com2,baudrate,tips,status.charAt(0),controlTime);
            num = comService.updateCom(com1,1);
        }catch (Exception e){
            e.printStackTrace();
        }
        return num;
    }
    @PostMapping("/delCom")
    @ResponseBody
    public int delCom(@RequestBody Map<String, String> com){
        int num = 0;
        try {
            int id = Integer.parseInt(com.get("DID"));
            String com2 = com.get("com")==null?"":com.get("com");
            String baudrate =  com.get("baudrate")==null?"":com.get("baudrate");
            String controlTime =  com.get("controlTime")==null?"":com.get("controlTime");
            String salt = com.get("salt")==null?"":com.get("salt");
            String status =  com.get("status")==null?"1":com.get("status");
            Com com1 = new Com(id,com2,baudrate,status.charAt(0),controlTime);
            num = comService.deleteCom(com1,1);
        }catch (Exception e){
            e.printStackTrace();
        }
        return num;
    }

}
