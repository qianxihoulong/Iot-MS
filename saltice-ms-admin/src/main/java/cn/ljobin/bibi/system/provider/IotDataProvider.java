package cn.ljobin.bibi.system.provider;

import cn.ljobin.bibi.api.IotDataApi;
import cn.ljobin.bibi.domain.Torrent;
import cn.ljobin.bibi.feign.AuthUserComsumer;
import cn.ljobin.bibi.service.IEnvironlService;
import cn.ljobin.bibi.service.ITorrentService;
import cn.ljobin.bibi.utils.AnnotationUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @program: IoT-Plat
 * @description:
 * @author: Mr.Liu
 * @create: 2020-04-11 15:28
 **/
@RestController
@Slf4j
public class IotDataProvider implements IotDataApi {
    @Autowired
    private ITorrentService iTorrentService;
    @Autowired
    private IEnvironlService iEnvironlService;
    @Autowired
    private AuthUserComsumer authUserComsumer;
    @Override
    public List<Torrent> getTorrentList(@RequestBody Torrent torrent) {
        QueryWrapper<Torrent> wrapper = new QueryWrapper<>();
        AnnotationUtils<Torrent> annotationUtils = new AnnotationUtils<>();
        wrapper = annotationUtils.wapperData(wrapper,torrent);
        return iTorrentService.list(wrapper);
    }
}
