/**
 * Copyright 2018-2020 stylefeng & fengshuonan (https://gitee.com/stylefeng)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.ljobin.bibi.system.provider;

import cn.ljobin.bibi.feign.AuthUserComsumer;
import cn.stylefeng.roses.core.base.controller.BaseController;
import cn.stylefeng.roses.core.datascope.DataScope;
import cn.stylefeng.roses.core.exception.AuthExceptionEnum;
import cn.stylefeng.roses.core.util.HttpContext;
import cn.stylefeng.roses.core.util.ToolUtil;
import cn.stylefeng.roses.kernel.model.exception.ServiceException;
import cn.stylefeng.roses.kernel.model.auth.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.naming.NoPermissionException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * 系统管理员控制器
 *
 * @author fengshuonan
 * @Date 2017年1月11日 下午1:08:17
 */
@Controller
@RequestMapping("/kf")
public class KfController extends BaseController {

    private static String PREFIX = "/system/kf/";
    @Autowired
    private AuthUserComsumer authUserComsumer;
    /**
     * 跳转到 新药方 页面
     */
    @RequestMapping("/newKf")
    public String newkf(HttpServletResponse response) {
        //return PREFIX + "kf.html";
        String sessionId =  UUID.randomUUID().toString().replace("-","");
        Cookie cookie=new Cookie("mySessionId",sessionId);
        cookie.setMaxAge(60*60*24*7);
        response.addCookie(cookie);
        return "redirect:http://47.103.1.210:8082/ENG/Madic/patient/patient.html";
    }
    /**
     * 跳转到 未完成药方 页面
     */
    @RequestMapping("/oldKf")
    public String oldkf() {
        return PREFIX + "old_kf.html";
    }
    /**
     * 跳转到添加药方条目的页面
     */
    @RequestMapping("/kf_add")
    public String kf_add() {
        return PREFIX + "kf_add.html";
    }
    /**
     * 修改管理员
     *
     * @throws NoPermissionException
     */
    @RequestMapping("/kf_edit")
    public String kf_edit(HttpServletRequest request, Model model) {
        String token = HttpContext.getTokenFromRequest(request);
        model.addAttribute("user",authUserComsumer.getUserByToken(token));
        return PREFIX + "kf_edit.html";
    }

    /**
     * 查看管理员详情
     */
    @RequestMapping("/view")
    @ResponseBody
    public SysUser view(HttpServletRequest request) {
        String token = HttpContext.getTokenFromRequest(request);
        return authUserComsumer.getUserByToken(token);
    }

}
