
package cn.ljobin.bibi.system.provider;

import cn.ljobin.bibi.api.IkfService;
import cn.ljobin.bibi.enums.MessageType;
import cn.ljobin.bibi.feign.AuthUserComsumer;
import cn.ljobin.bibi.model.*;
import cn.ljobin.bibi.netty.handler.RxtxHandler;
import cn.ljobin.bibi.netty.util.fileupload.FileUtil;
import cn.ljobin.bibi.system.activemqsend.ActiveMqMessageSender;
import cn.ljobin.bibi.utils.ReadExcel;
import cn.ljobin.bibi.utils.cache.Cache;
import cn.stylefeng.roses.core.base.controller.BaseController;
import cn.stylefeng.roses.core.reqres.response.ResponseData;
import cn.stylefeng.roses.core.util.HttpContext;
import cn.stylefeng.roses.core.util.ToolUtil;
import cn.stylefeng.roses.kernel.model.api.base.RpcBaseResponse;
import cn.stylefeng.roses.kernel.model.auth.SysUser;
import io.netty.channel.Channel;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import cn.ljobin.bibi.domain.monitor.MonitorMessage;
import cn.ljobin.bibi.domain.monitor.enums.LinkInfoType;
import static cn.ljobin.bibi.schedules.Schedule.addQueTask;


/**
 * 接口控制器提供
 *
 * @author stylefeng
 * @Date 2018/7/20 23:39
 */
@RestController
@RequestMapping("/h5")
public class H5ApiController extends BaseController {
    /**用来保存灾灾取药的 【账号：药方id】**/
    public static final Map<String,String> KFMAP = new HashMap<>();
    @Autowired
    IkfService ikfService;
    @Autowired
    AuthUserComsumer authUserComsumer;
    @Autowired
    private ActiveMqMessageSender activeMqMessageSender;
    /**
     * 药方单编号生成器
     * @return
     */
    public static String getUUID(){
        return UUID.randomUUID().toString().replaceAll("-","");
    }

    /**
     * 获取用户信息
     * @param request
     * @return
     */
    private SysUser getUserFromFeign(HttpServletRequest request){
        String token = HttpContext.getTokenFromRequest(request);
        return authUserComsumer.getUserByToken(token);
    }
    /**
     * 检查是否是药店管理员
     * @param token
     * @param role
     * @return
     */
    public  boolean chackRoot(String token,int role){
        RpcBaseResponse roles = authUserComsumer.getLoginUserByToken(token);
        AtomicInteger i= new AtomicInteger(0);
        ((List<Integer>)(roles.getData())).forEach(v->{
            // 7 为药店管理员的 id
            if(v == 7){
                i.getAndIncrement();
            }
        });
        if(i.get()>0){
            return true;
        }
        return false;
    }

    @GetMapping("/getBegin")
    public ResponseData getBegin(){
        String ms = ikfService.selectBeginPath(1);
        if(ms==null||"".equals(ms)){
            return ResponseData.error("当前药方没有起始点");
        }
        int[] data = new int[3];
        String[] d = ms.split("-->");
        data[0] = Integer.parseInt(d[0].substring(3));
        String[] ad = d[1].split("，");
        data[1] = Integer.parseInt(ad[0].substring(1,ad[0].length()-1));
        data[2] = Integer.parseInt(ad[1].substring(1,ad[1].length()-1));
        return ResponseData.success(data);
    }
    @GetMapping("/updateBegin/{B}/{H}/{L}")
    public ResponseData updateBegin(@PathVariable("B")String b, @PathVariable("H")String h, @PathVariable("L")String l){
        if(isStr2Num(b)&&isStr2Num(h)&&isStr2Num(l)){
            String begin = "中药柜"+b+"-->第"+h+"行，第"+l+"列";
            return ResponseData.success(ikfService.updateBeginPath(1,begin));
        }
        return ResponseData.error("数据不为数字");

    }

    /**
     * 检查字符串是否可转为数字
     * @param msg
     * @return
     */
    public static boolean isStr2Num(String msg){
        try {
            Integer.parseInt(msg);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
    /**
     * kf 检查路径规划是否完成
     * @param kf
     * @return
     */
    @GetMapping("/getPath/{kf}")
    public ResponseData getPath(@PathVariable("kf") String kf){
        if(Cache.get("kfPath"+kf)==null){
            return new ResponseData(true,406,"请稍等，服务器正在规划取药路径",null);
        }
        return new ResponseData(true,200,"seccess",null);
    }
    /**
     * kf 1:开方的时候返回药名list  0:返回数据库原始数据
     * @param kf
     * @return
     */
    @GetMapping("/yao/{kf}")
    public ResponseData yao(@PathVariable("kf") String kf,HttpServletRequest request){
        List<MAddress> listM = ikfService.selectYao(1);
        if("1".equals(kf)){
            List<String> list = new ArrayList<>();
            listM.forEach(k->{
                list.add(k.getmName());
            });
            SysUser user = getUserFromFeign(request);
            if(ToolUtil.isEmpty(user)){
                return new ResponseData(true,501,"服务繁忙",null);
            }
            String name = user.getName();
            return new ResponseData(true,200,name,list);
        }
        return new ResponseData(true,200,"seccess",listM);
    }
    /**
     * id 获取当前病人的基本信息与药品数据
     * @param id
     * @return
     */
    @GetMapping("/yaos/{id}")
    public ResponseData yaos(@PathVariable("id") Long id,HttpServletRequest request){
        SysUser user = getUserFromFeign(request);
        if(ToolUtil.isEmpty(user)){
            return new ResponseData(true,501,"服务繁忙",null);
        }
        String name = user.getName();
        List<MAddress> listM = ikfService.selectYao(1);
        Map<String , Object> map = new HashMap<>(2);
        List<String> list = new ArrayList<>();
        listM.forEach(k->{
            list.add(k.getmName());
        });
        Patient_info patientInfo = ikfService.selectPatientById(id);
        map.put("list",list);
        map.put("info",patientInfo);
        return new ResponseData(true,200,name,map);
    }
    /**
     * id 获取当前病人的全部基本信息
     * @param id
     * @return
     */
    @GetMapping("/patientInfo/{id}")
    public ResponseData patientInfo(@PathVariable("id") String id){
        if(id==null || "".equals(id)){
            return ResponseData.error("信息错误");
        }
        Long ids = 0L;
        try{
            ids = Long.parseLong(id);
        }catch (Exception e){
            return ResponseData.error("信息错误");
        }
        Patient_info patientInfo = ikfService.selectPatientAllInfoById(ids);
        //String dockerName = iUserService.getNameById(patientInfo.getDid());
        return ResponseData.success(200,"dockerName",patientInfo);
    }
    @GetMapping("/yao2/{kf}")
    public ResponseData yao2(@PathVariable("kf") String kf,HttpServletRequest request){
        List<MAddress> listM = ikfService.selectYao(1);
        if("1".equals(kf)){
            List<String> list = new ArrayList<>();
            listM.forEach(k->{
                list.add(k.getmName());
            });
            SysUser user = getUserFromFeign(request);
            if(ToolUtil.isEmpty(user)){
                return new ResponseData(true,501,"服务繁忙",null);
            }
            String name = user.getName();
            return new ResponseData(true,200,name,list);
        }
        return new ResponseData(true,200,"seccess",listM);
    }

    /**
     * 获取所有的药方
     * @return
     */
    @GetMapping("/kf_info")
    public ResponseData kf_info(){
        try {
            List<Kf> kfs = ikfService.kfInfo();
            return new ResponseData(true,200,"seccess",kfs);
        }catch (Exception e){
            return new ResponseData(false,500,"error",e.getMessage());
        }
    }

    /**
     * 获取所有的病人信息 基本信息
     * @return
     */
    @GetMapping("/patient_info")
    public ResponseData patient_info(HttpServletRequest request){
        SysUser user = getUserFromFeign(request);
        if(ToolUtil.isEmpty(user)){
            return new ResponseData(true,501,"服务繁忙",null);
        }
        System.err.println(user.toString());
        List<Patient_info> kfs = ikfService.patient_info(user.getUserId());;
        return new ResponseData(true,200,"seccess",kfs);
    }
    /**
     * 根据病人id获取所有详细数据
     * @param id
     * @return
     */
    @GetMapping("/patient_kf_getByid/{id}")
    public ResponseData patient_kf_getByid(@PathVariable("id") Long id){
        return new ResponseData(true,200,"获取成功",ikfService.kfInfoById_root(id));
    }
    /**
     * 根据kfid获取所有详细数据
     * @param kfid
     * @return
     */
    @GetMapping("/kf_getByid/{kfid}")
    public ResponseData kf_getByid(@PathVariable("kfid") String kfid){
        try {
            List<MStream> kfs = ikfService.yaoGetByKid(kfid);
            Kf kf =  ikfService.kfGetByKid(kfid);
            Advise advise = ikfService.adviseGetByKid(kfid);
            kf.setmStreams(kfs);
            kf.setAdvise(advise);
           // String dockerName = iUserService.getNameById(kf.getDid());
            return new ResponseData(true,200,"dockerName",kf);
        }catch (Exception e){
            e.printStackTrace();
            return new ResponseData(false,500,"error",null);
        }
    }
    /**
     * 根据kfid获取所有详细数据 升级版
     * @param kfid
     * @return
     */
    @GetMapping("/Patient_kf_getByid/{kfid}")
    public ResponseData Patient_kf_getByid(@PathVariable("kfid") String kfid){
        try {
            List<MStream> kfs = ikfService.yaoGetByKid(kfid);
            PatientPrescription prescription =  ikfService.selectPatientKfByKfId(kfid);
            Patient_info patientInfo = ikfService.selectPatientById(prescription.getPid());
            Advise advise = ikfService.adviseGetByKid(kfid);
            patientInfo.setMStreams(kfs);
            patientInfo.setAdvise(advise);
            patientInfo.setPatientPrescription(prescription);
            //String dockerName = iUserService.getNameById(prescription.getDid());
            return new ResponseData(true,200,"dockerName",patientInfo);
        }catch (Exception e){
            e.printStackTrace();
            return new ResponseData(false,500,"error",null);
        }
    }

    /**
     * 根据kfid获取要开的药
     * @param kfid
     * @return
     */
//    @CrossOrigin
    @GetMapping("/kf_yao_getByid/{kfid}")
    public ResponseData kf_yao_getByid(@PathVariable("kfid") String kfid,HttpServletRequest request){
        try {
           // 放入全局保存 【账号：开方id】 要是没有获取到账号则置为0
            SysUser user = getUserFromFeign(request);
            if(ToolUtil.isEmpty(user)){
                return new ResponseData(true,406,"服务繁忙",null);
            }
            if(Cache.get("kfPath"+kfid)==null){
                return new ResponseData(true,406,"请稍等，服务器正在快马加鞭计算",null);
            }
            int[] path = (int[])Cache.get("kfPath"+kfid);
            List<MStream> kfs = ikfService.getMedicine(kfid);
            AtomicInteger is= new AtomicInteger(0);
            //对取药顺序进行 优化 计算出最优路径
            kfs.forEach(v->{
                v.setId(path[is.getAndIncrement()]+1);
            });
            //按序号升序排好
            kfs = kfs.stream().sorted(Comparator.comparing(MStream::getId)).collect(Collectors.toList());
            KFMAP.put(user.getUserId().toString(),kfid);
            Channel channel = (Channel) Cache.get("channel");
            if(channel!=null&& channel.isWritable()){
                RxtxHandler.writeAndFlushNettyUpdateKf(channel,user.getUserId().toString());
                return new ResponseData(true,200,"seccess",kfs);
            }
            return new ResponseData(true,200,"error",kfs);

        }catch (Exception e){
            e.printStackTrace();
            return new ResponseData(false,500,"error",null);
        }
    }
    /**
     * 根据kfid获取要开的药的规划好的数据 本地电脑调用的
     * @param uid
     * @return
     */
    @CrossOrigin
    @GetMapping("/kf_yao_getByid_loc/{uid}")
    public ResponseData kf_yao_getByid_loc(@PathVariable("uid") String uid){
        String kfid = H5ApiController.KFMAP.get(uid);
        if( kfid == null| "".equals(kfid)){
            return ResponseData.error(400,"未获取到要取药方的信息",new ArrayList<MStream>());
        }
        List<MStream> kfs = ikfService.getMedicine(kfid);
        //下面升级到了2版本
        String name = ikfService.selectNameByKfId2(kfid);
        AtomicInteger is= new AtomicInteger(0);
        if(Cache.get("kfPath"+kfid)==null){
            return new ResponseData(true,406,"请稍等，服务器正在快马加鞭计算",null);
        }
        int[] path = (int[])Cache.get("kfPath"+kfid);
        //对取药顺序进行 优化 计算出最优路径
        kfs.forEach(v->{
            v.setId(path[is.getAndIncrement()]+1);
        });
        //按序号升序排好
        kfs = kfs.stream().sorted(Comparator.comparing(MStream::getId)).collect(Collectors.toList());
        return ResponseData.success(200,name+"::"+uid,kfs);
    }
    /**
     * 根据kfid获取要开的药
     * @param kfid
     * @return
     */
    @GetMapping("/kf_yao_getByid_box/{kfid}")
    public ResponseData kf_yao_getByid_box(@PathVariable("kfid") String kfid){
        try {
            Map<String,List<BoxContent>> map = new HashMap<>();

            if(Cache.get("kfid2D:"+kfid)==null){
                List<MStream> kfs = ikfService.getMedicine(kfid);
                AtomicInteger is= new AtomicInteger(0);
                if(Cache.get("kfPath"+kfid)==null){
                    return new ResponseData(true,406,"请稍等，服务器正在快马加鞭计算",null);
                }
                int[] path = (int[])Cache.get("kfPath"+kfid);
                //对取药顺序进行 优化 计算出最优路径
                kfs.forEach(v->{
                    v.setId(path[is.getAndIncrement()]+1);
                    System.out.println(v.toString());
                });
                int kfsLength=kfs.size();
                map = ikfService.getAllMA();
                for (int i = 0; i < kfsLength; i++) {
                    int lengths = map.get(kfs.get(i).getTips().split("-->")[0]).size();
                    for (int j = 0; j < lengths; j++) {
                        if(kfs.get(i).getM_name().equals(map.get(kfs.get(i).getTips().split("-->")[0]).get(j).getYaoName())){
                            map.get(kfs.get(i).getTips().split("-->")[0]).get(j).setOrder(kfs.get(i).getId());
                            map.get(kfs.get(i).getTips().split("-->")[0]).get(j).setTag(1);
                        }
                    }
                }
                //保存15天
                Cache.put("kfid2D:"+kfid,map,60*60*24*15,true);
            }else {
                map = (Map<String,List<BoxContent>>) Cache.get("kfid2D:"+kfid);
            }
            return new ResponseData(true,200,map.size()+"",map);
        }catch (Exception e){
            e.printStackTrace();
            return new ResponseData(false,500,"服务器跑路啦！",null);
        }
    }

    /**
     * 更新取完的药物
     * @param ids
     * @return
     */
    @GetMapping("/updateVersion_kf_yao/{ids}/{kfid}")
    public ResponseData kf_yap_updateVersion(@PathVariable("ids") String ids, @PathVariable("kfid") String kfid){
        String[] id = ids.trim().split(",");
        int length = id.length;
        List<Integer> idsList = new ArrayList<>();
        try {
            for (int i=0;i<length;i++){
                idsList.add(Integer.parseInt(id[i]));
            }
            return new ResponseData(true,200,"seccess",ikfService.updateAllMedicineVersion(idsList,kfid));
        }catch (Exception e){
            e.printStackTrace();
            return new ResponseData(false,500,"error",null);
        }
    }
    @PostMapping("/froms")
    public ResponseData fromIn(HttpServletRequest request){//@RequestBody JSONObject jsonParam
        int length = Integer.parseInt(request.getParameter("neirongLength") == null||"".equals(request.getParameter("neirongLength"))?"0":request.getParameter("neirongLength"));
        Map<String,String[]> map = request.getParameterMap();

         String dName = request.getParameter("name") == null?"无":request.getParameter("name");
         String sex = request.getParameter("sex") == null?"无":request.getParameter("sex");
         Integer age = Integer.valueOf(request.getParameter("age") == null||"".equals(request.getParameter("age"))?"0":request.getParameter("age"));
         Float moon = Float.valueOf(request.getParameter("agemoon") == null||"".equals(request.getParameter("agemoon"))?"0":request.getParameter("agemoon"));
         String kfTime = request.getParameter("time") == null?"0":request.getParameter("time");
         String address = request.getParameter("address") == null?"无":request.getParameter("address");
         String diagnosis = request.getParameter("diagnosis") == null?"无":request.getParameter("diagnosis");
         String cash = request.getParameter("cash") == null?"0":request.getParameter("cash").trim();
        String advice = request.getParameter("advice") == null?"":request.getParameter("advice").trim();
         String kfId = getUUID();
        //这个编号，暂时用输入的innerDoctor（其姓名）代替
         //Integer did = Integer.valueOf(request.getParameter("innerDoctor") == null||"".equals(request.getParameter("innerDoctor"))?"0":request.getParameter("innerDoctor"));
        SysUser user = getUserFromFeign(request);
        Long did =  user.getUserId();
         Kf kf = new Kf(dName,sex,age,moon,kfTime,address,diagnosis,kfId,did,cash);
         String[] adviseData = advice.split("--");
         if (adviseData.length!=5){
             return new ResponseData(false,406,"医嘱数据异常",null);
         }
        Advise advise = new Advise(kfId,Integer.parseInt(adviseData[0].trim()),adviseData[1].trim(),
                adviseData[2].trim(),adviseData[3].trim(),adviseData[4].trim());
        List<MStream> neiRongList = new ArrayList<>(length);
        for (int i = 0; i < length; i++) {
            String name = map.get("neirong["+i+"][yaoName]")[0];
            String liang = map.get("neirong["+i+"][yaoLiang]")[0];
            String beiZhu = map.get("neirong["+i+"][beiZhu]")[0];
            neiRongList.add(new MStream(kfId,name,liang,beiZhu));
        }
        if(ikfService.insertKf(kf,neiRongList,advise)>0){
            addQueTask(kfId);
            return new ResponseData(true,200,"seccess",null);
        }
        return new ResponseData(false,400,"error",null);
    }
    @PostMapping("/kf_froms")
    public ResponseData kf_fromIn(HttpServletRequest request){//@RequestBody JSONObject jsonParam
        int length = Integer.parseInt(request.getParameter("neirongLength") == null||"".equals(request.getParameter("neirongLength"))?"0":request.getParameter("neirongLength"));
        Map<String,String[]> map = request.getParameterMap();
        String kfTime = request.getParameter("time") == null?"0":request.getParameter("time");
        String diagnosis = request.getParameter("diagnosis") == null?"无":request.getParameter("diagnosis");
        String cash = request.getParameter("cash") == null?"0":request.getParameter("cash").trim();
        String advice = request.getParameter("advice") == null?"":request.getParameter("advice").trim();
        Long id = Long.parseLong(request.getParameter("id") == null?"0":request.getParameter("id").trim());
        String kfId = getUUID();
        SysUser user = getUserFromFeign(request);
        if(ToolUtil.isEmpty(user)){
            return new ResponseData(false,406,"服务繁忙",null);
        }
        Long did =  user.getUserId();
        PatientPrescription prescription = new PatientPrescription(id,kfTime,diagnosis,kfId,did,"0",cash,"1");
        String[] adviseData = advice.split("--");
        if (adviseData.length!=5){
            return new ResponseData(false,406,"医嘱数据异常",null);
        }
        Advise advise = new Advise(kfId,Integer.parseInt(adviseData[0].trim()),adviseData[1].trim(),
                adviseData[2].trim(),adviseData[3].trim(),adviseData[4].trim());
        List<MStream> neiRongList = new ArrayList<>(length);
        for (int i = 0; i < length; i++) {
            String name = map.get("neirong["+i+"][yaoName]")[0];
            String liang = map.get("neirong["+i+"][yaoLiang]")[0];
            String beiZhu = map.get("neirong["+i+"][beiZhu]")[0];
            neiRongList.add(new MStream(kfId,name,liang,beiZhu));
        }
        if(ikfService.insertKf2(prescription,neiRongList,advise)>0){
            addQueTask(kfId);
            MonitorMessage monitorMessage = new MonitorMessage();
            monitorMessage.setClientId("satice");
            monitorMessage.setMessageType(MessageType.SUCCESS);
            monitorMessage.setTitle("有小偷，快跑，测试。。。");
            monitorMessage.setLinkInfo("1337792659@qq.com");
            monitorMessage.setLinkInfoType(LinkInfoType.EMAIL);
            monitorMessage.setType(LinkInfoType.EMAIL.getId());
            monitorMessage.setPloy("提交药方成功");
            activeMqMessageSender.sendMessage(monitorMessage);
            return new ResponseData(true,200,"seccess",null);
        }
        return new ResponseData(false,400,"error",null);
    }

    /**
     * 添加病人信息
     * @param request
     * @return
     */
    @PostMapping("/patient_info_from")
    public ResponseData patient_info_from(HttpServletRequest request){//@RequestBody JSONObject jsonParam
        String name = request.getParameter("name") == null?"":request.getParameter("name").trim();
        String sex = request.getParameter("sex") == null?"男":request.getParameter("sex").trim().substring(0,1);
        String birthday = request.getParameter("birthday") == null?"":request.getParameter("birthday").trim();
        String address = request.getParameter("address") == null?"":request.getParameter("address").trim();
        String phone = request.getParameter("phone") == null?"":request.getParameter("phone").trim();
        int height = Integer.parseInt(request.getParameter("height") == null?"0":request.getParameter("height").trim());
        int weight = Integer.parseInt(request.getParameter("weight") == null?"0":request.getParameter("weight").trim());
        SysUser user = getUserFromFeign(request);
        if(ToolUtil.isEmpty(user)){
            return new ResponseData(true,501,"服务繁忙",null);
        }
        Long did =  user.getUserId();
        Patient_info patientInfo = new Patient_info(name,sex,birthday,address,height,weight,did,phone);
        if(ikfService.insertPatientInfo(patientInfo)>0){
            return new ResponseData(true,200,"seccess",null);
        }
        return new ResponseData(false,400,"error",null);
    }

    /**
     * 编辑病人信息
     * @param request
     * @return
     */
    @PostMapping("/edit_patient_info_from")
    public ResponseData edit_patient_info_from(HttpServletRequest request){//@RequestBody JSONObject jsonParam
        SysUser user = getUserFromFeign(request);
        String ids = request.getParameter("id") == null?"无":request.getParameter("id").trim();
        int id = Integer.parseInt(ids);
        String dids = request.getParameter("did") == null?"0":request.getParameter("did").trim();
        Long did = Long.parseLong(dids);
        if(!did.equals(user.getUserId())){
            return ResponseData.error(406,"您不是对应的医师");
        }
        String name = request.getParameter("name2") == null?"":request.getParameter("name2").trim();
        String phone = request.getParameter("phone") == null?"":request.getParameter("phone").trim();
        String sex = request.getParameter("sex") == null?"男":request.getParameter("sex").trim().substring(0,1);
        String birthday = request.getParameter("birthday") == null?"":request.getParameter("birthday").trim();
        String address = request.getParameter("address") == null?"":request.getParameter("address").trim();
        int height = Integer.parseInt(request.getParameter("height") == null?"0":request.getParameter("height").trim());
        int weight = Integer.parseInt(request.getParameter("weight") == null?"0":request.getParameter("weight").trim());

        Patient_info patientInfo = new Patient_info(id,name,sex,birthday,address,height,weight,did,phone);
        if(ikfService.updatePatientInfo(patientInfo)>0){
            return new ResponseData(true,200,"seccess",null);
        }
        return new ResponseData(false,400,"error",null);
    }
    @PostMapping("/froms2")
    public ResponseData fromIn2(HttpServletRequest request){//@RequestBody JSONObject jsonParam
        int length = Integer.parseInt(request.getParameter("neirongLength") == null||"".equals(request.getParameter("neirongLength"))?"0":request.getParameter("neirongLength"));
        Map<String,String[]> map = request.getParameterMap();

        String dName = request.getParameter("name") == null?"无":request.getParameter("name");
        String sex = request.getParameter("sex") == null?"无":request.getParameter("sex");
        Integer age = Integer.valueOf(request.getParameter("age") == null||"".equals(request.getParameter("age"))?"0":request.getParameter("age"));
        Float moon = Float.valueOf(request.getParameter("agemoon") == null||"".equals(request.getParameter("agemoon"))?"0":request.getParameter("agemoon"));
        String kfTime = request.getParameter("time") == null?"0":request.getParameter("time");
        String address = request.getParameter("address") == null?"无":request.getParameter("address");
        String diagnosis = request.getParameter("diagnosis") == null?"无":request.getParameter("diagnosis");
        String cash = request.getParameter("cash") == null?"0":request.getParameter("cash").trim();
        String advice = request.getParameter("advice") == null?"":request.getParameter("advice").trim();
        String kfId = getUUID();
        //这个编号，暂时用输入的innerDoctor（其姓名）代替
        //Integer did = Integer.valueOf(request.getParameter("innerDoctor") == null||"".equals(request.getParameter("innerDoctor"))?"0":request.getParameter("innerDoctor"));
        SysUser user = getUserFromFeign(request);
        Long did =  user.getUserId();
        Kf kf = new Kf(dName,sex,age,moon,kfTime,address,diagnosis,kfId,did,cash);
        System.err.println(advice);
        String[] adviseData = advice.split("--");
        if (adviseData.length!=5){
            return new ResponseData(false,406,"医嘱数据异常",null);
        }
        Advise advise = new Advise(kfId,Integer.parseInt(adviseData[0].trim()),adviseData[1].trim(),
                adviseData[2].trim(),adviseData[3].trim(),adviseData[4].trim());
        List<MStream> neiRongList = new ArrayList<>(length);
        for (int i = 0; i < length; i++) {
            String name = map.get("neirong["+i+"][yaoName]")[0];
            String liang = map.get("neirong["+i+"][yaoLiang]")[0];
            String beiZhu = map.get("neirong["+i+"][beiZhu]")[0];
            neiRongList.add(new MStream(kfId,name,liang,beiZhu));
        }
        if(ikfService.insertKf(kf,neiRongList,advise)>0){
            addQueTask(kfId);
            return new ResponseData(true,200,"seccess",null);
        }
        return new ResponseData(false,400,"error",null);
    }

    @PostMapping("/excel/up")
    public ResponseData upExcel(@RequestParam("excel") MultipartFile file){
        try {
            String path = FileUtil.saveImg(file);
            System.out.println("》》》》》》》》》》》excel上传保存成功："+path);
            if(path!=null){
                ReadExcel excel = new ReadExcel();
                String code = excel.importExcel(new FileInputStream(new File(path)),path.trim().split("/")[path.trim().split("/").length-1]);
                if("200".equals(code)){
                    ConcurrentHashMap<String,String> concurrentHashMap = (ConcurrentHashMap<String,String>) Cache.get(ReadExcel.EXCEL_CACHE);
                    if (concurrentHashMap != null) {
                        System.out.println("获取到"+ concurrentHashMap.size()+"行数据");
//                        concurrentHashMap.forEach((k, v)->{
//                            System.out.println(k+"--->>>>"+v);
//                        });
                        int num = ikfService.updataMAddressHshMap(concurrentHashMap);
                        return new ResponseData(true,200,"seccess",num);
                    }else {
                        return new ResponseData(false,401,"未获取到数据",null);
                    }

                }else {
                    int num = Integer.parseInt(code)-200;
                    return new ResponseData(false,400,"excel文件内容格式不对，请检查第"+num+"行数据",null);
                }
            };
//            ReadExcel excelReader = new ReadExcel(path)
            // 对读取Excel表格标题测试
//			String[] title = excelReader.readExcelTitle();
//			System.out.println("获得Excel表格的标题:");
//			for (String s : title) {
//				System.out.print(s + " ");
//			}

//            // 对读取Excel表格内容测试
//            Map<Integer, Map<Integer,Object>> map = excelReader.readExcelContent();
//            System.out.println("获得Excel表格的内容:");
//            List<MAddress> mAddressList = new ArrayList<>();
//            for (int i = 1; i <= map.size(); i++) {
//
//                List<String> list = new ArrayList<>(3);
//                map.get(i).values().forEach(v->{
//                    list.add( v==null || "".equals((String)v)?"0":(String) v);
//                });
//                if(list.size()!=3){
//                    System.err.println("列数不对");
//                    return new ResponseData(false,406,"excel文件内容格式不对，请下载模板填写数据",null);
//                }
//                float mlinag ;
//                try {
//                    mlinag = Float.parseFloat(list.get(2));
//                }catch (Exception e){
//                    return new ResponseData(false,402,"入库药量请填写数字，单位/kg",null);
//                }
//                MAddress mAddress = new MAddress(list.get(0),list.get(1),1,mlinag);
//                mAddressList.add(mAddress);
//            }
            //int num = ikfService.updateMAddressList(mAddressList);
            return new ResponseData(false,500,"excel上传保存路径错误，请联系管理员",null);
        } catch (FileNotFoundException e) {
            System.out.println("未找到指定路径的文件!");
            e.printStackTrace();
            return new ResponseData(false,500,"error",null);
        }catch (Exception e) {
            e.printStackTrace();
            return new ResponseData(false,500,"error",null);
        }
    }
    @GetMapping(value = "/excel/export")
    public void exportExcel(HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        List<Student> list = new ArrayList<Student>();
        list.add(new Student(1000,"zhangsan","20"));
        list.add(new Student(1001,"lisi","23"));
        list.add(new Student(1002,"wangwu","25"));
        HSSFWorkbook wb = export(list);
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment;filename=student.xls");
        OutputStream ouputStream = response.getOutputStream();
        wb.write(ouputStream);
        ouputStream.flush();
        ouputStream.close();
    }
    class Student{
        int id;
        String name;
        String age;

        public Student(int id, String name, String age) {
            this.id = id;
            this.name = name;
            this.age = age;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }
    }
    String[] excelHeader = { "名称", "入库量","位置"};
    public HSSFWorkbook export(List<Student> list) {
        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet = wb.createSheet("中药库存清单");
        HSSFRow row = sheet.createRow((int) 0);
        HSSFCellStyle style = wb.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);
        // 设置这些样式
        style.setFillForegroundColor(HSSFColor.SKY_BLUE.index);
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        style.setBorderRight(HSSFCellStyle.BORDER_THIN);
        style.setBorderTop(HSSFCellStyle.BORDER_THIN);

        for (int i = 0; i < excelHeader.length; i++) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue(excelHeader[i]);
            cell.setCellStyle(style);
            sheet.autoSizeColumn(i);
            sheet.setColumnWidth(i,2500);

        }

//        for (int i = 0; i < list.size(); i++) {
//            row = sheet.createRow(i + 1);
//            Student student = list.get(i);
//            row.createCell(0).setCellValue(student.getId());
//            row.createCell(1).setCellValue(student.getName());
//            row.createCell(2).setCellValue(student.getAge());
//        }
        return wb;
    }
}

