package cn.ljobin.bibi.roses.schedule;

import cn.ljobin.bibi.common.RedisUtil;
import cn.ljobin.bibi.domain.Energy;
import cn.ljobin.bibi.domain.Environl;
import cn.ljobin.bibi.domain.Torrent;
import cn.ljobin.bibi.service.IEnergyService;
import cn.ljobin.bibi.service.IEnvironlService;
import cn.ljobin.bibi.service.ITorrentService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

/**
 * @program: ruoyi
 * @description: redis中终端数据自动保存
 * @author: Mr.Liu
 * @create: 2020-03-24 15:24
 **/
@Component
@EnableScheduling
@Controller
public class RedisDataSave2 {
    @Value("${spring.redis.isOpen}")
    private boolean isOpen = false;

    private static final Logger logger = LoggerFactory.getLogger(RedisDataSave2.class);
    @Autowired
    private IEnvironlService tbEnvironmentService;
    @Autowired
    private ITorrentService tbIoterminalService;
    @Autowired
    private IEnergyService tbEnergyService;
    @Autowired
    private RedisUtil redisUtil;

    /**
     * 终端实现，下线的处理
     * 0.1s一次
     */
    @Scheduled(fixedRate = 100)
    public void autoSaveRedisIOT(){
        if(!isOpen){
            return;
        }else {
                //logger.info("终端实现，下线的处理1");
                Object o = redisUtil.listLiftPop("iot_ioterminal");
                if(o!=null){
                    tbIoterminalService.onlineOrDisOnline(redisUtil.fromJson((String) o, Torrent.class));
                    System.out.println("1");
                }
                //logger.info("终端实现，下线的处理2");

        }


    }

    /**
     * 终端环境数据的处理
     *  0.05s一次
     */
    @Scheduled(fixedRate = 50)
    public void autoSaveRedisENV(){
        if(!isOpen){
            return;
        }else {

                //logger.info("终端环境数据的处理1");
                Object o = redisUtil.listLiftPop("iot_environment");
                if(o!=null){
                    tbEnvironmentService.insert(redisUtil.fromJson((String) o, Environl.class));
                    System.out.println("2");
                }
                //logger.info("终端环境数据的处理2");

        }

    }
    /**
     * 终端能源数据的处理
     *  0.05s一次
     */
    @Scheduled(fixedRate = 50)
    public void autoSaveRedisEnergy(){
        if(!isOpen){
            return;
        }

            //logger.info("终端环境数据的处理1");
            Object o = redisUtil.listLiftPop("iot_energy");
            if(o!=null){
                tbEnergyService.insert(redisUtil.fromJson((String) o, Energy.class));
                System.out.println("3");
            }
            //logger.info("终端环境数据的处理2");

    }
}
