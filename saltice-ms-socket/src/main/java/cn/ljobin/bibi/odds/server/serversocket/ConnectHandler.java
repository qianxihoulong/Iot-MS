package cn.ljobin.bibi.odds.server.serversocket;

import cn.ljobin.bibi.odds.Const;
import cn.ljobin.bibi.odds.MysqlImages;
import cn.ljobin.bibi.odds.inboundhandler.MySimpleChannelInboundHandler;
import cn.ljobin.bibi.odds.server.HeartBeatServer;
import cn.ljobin.bibi.odds.server.devsocket.ChannelManage;
import cn.ljobin.bibi.odds.server.devsocket.DevConnectHandler;
import cn.ljobin.bibi.odds.server.devsocket.HeartbeatServerHandler;
import cn.ljobin.bibi.odds.service.impl.DataServiceImpl;
import cn.ljobin.bibi.odds.unity.SpringContextByClassUtil;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author liuyanbin
 */
public class ConnectHandler extends MySimpleChannelInboundHandler<Object> {

    private static final Logger logger = LoggerFactory.getLogger(ConnectHandler.class);

    private String times = "QUERY TIME ORDER";
    private String beginL = "$";
    private String beginS = "s";
    private String tID = ":";
    private String endl = "#";
    public static String end = "@";
    public static String serverName = "server";
    private String devName = "dev:";
    //开启录制
    private String videoLu = "1";
    /**
     * AtomicInteger满足在高并发的情况下,原生的整形数值自增线程不安全的问题
     */
    /**控制最大连接数量**/
    private AtomicInteger connectNum;
    /**当前最大连接数量**/
    private AtomicInteger nowConnectNum;
    //设备上传的数据消息
    /** $+设备编号+ : +地址+功能+数据位+数据+校验位+# **/
    //后台服务器向设备发送消息
    /** S+设备编号+ : +地址+功能+数据位+数据+校验位+# **/
    /**统计用户发送消息次数**/
    private int counter;
    /**花式注入redis服务，不能用@Autowired注入**/
    private static DataServiceImpl redisUtil;
    static {
        redisUtil = SpringContextByClassUtil.getBean(DataServiceImpl.class);
    }

    public ConnectHandler(AtomicInteger connectNum) {
        this.connectNum = connectNum;
    }


public static String cheack(String body,ChannelHandlerContext ctx){
        String[] s = body.substring(0,body.length()-2).split(":");
        if(s.length<3){
            return Const.BAD_FORMAT;
        }
        ChannelManage.userIdAndChannelMap.get(s[1]).writeAndFlush(DevConnectHandler.formatData(s[2]));
        return Const.S_INT;
}
    @Override
    protected void messageRead(ChannelHandlerContext ctx, Object msg) {
        Channel incoming = ctx.channel();
        String body = (String) msg;
        if("pingDev1024#".equals(msg)){
            ByteBuf resp = Unpooled.copiedBuffer((msg+":"+ HeartbeatServerHandler.onLineNum+end).getBytes());
            incoming.writeAndFlush(resp);
        }else if("server#".equals(msg)){
            if(!HeartbeatServerHandler.serverAdd(incoming)){
                String datas = Const.E_INT ;
                ctx.writeAndFlush(DevConnectHandler.formatData(datas+end));
                ctx.close();
            }else {
                //获取客户端ip
                InetSocketAddress ipSocket = (InetSocketAddress)ctx.channel().remoteAddress();
                String clientIp = ipSocket.getAddress().getHostAddress();
                //System.getProperty("line.separator") 这也是换行符,功能和"\n"是一致的,但是此种写法屏蔽了 Windows和Linux的区别 ，更保险一些
                //+ System.getProperty("line.separator")
                String datas = "<<<echo to "+clientIp+"--绑定成功>>>"+ Const.S_INT ;
                ctx.writeAndFlush(DevConnectHandler.formatData(datas+end));
            }
        } else{
            String currentTime;
            System.out.println("Client:"+incoming.remoteAddress()+" ::"+body+"; the counter is :"+ ++counter+">>>"+ HeartBeatServer.dataTime());
            //数据效验
            currentTime =dataFormatCheck(body,ctx);

            //获取客户端ip
            InetSocketAddress ipSocket = (InetSocketAddress)ctx.channel().remoteAddress();
            String clientIp = ipSocket.getAddress().getHostAddress();

            //System.getProperty("line.separator") 这也是换行符,功能和"\n"是一致的,但是此种写法屏蔽了 Windows和Linux的区别 ，更保险一些
            //+ System.getProperty("line.separator")
            currentTime = "<<<echo to "+clientIp+">>>"+currentTime ;
            ByteBuf resp = Unpooled.copiedBuffer((currentTime+end).getBytes());
            ctx.writeAndFlush(resp);
        }


//        一个一个ByteBuf接收
//        ByteBuf in = (ByteBuf) msg;
//        try {
//            while (in.isReadable()) { // (1)
//                System.out.println((char) in.readByte());
//                System.out.flush();
//            }
//        } finally {
//            ReferenceCountUtil.release(msg); // (2)
//        }
    }
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx , Throwable cause){
        logger.error(ctx.channel().id()+"关闭连接"+">>>"+ HeartBeatServer.dataTime());

        ctx.close();
    }
    @Override
    public void channelRegistered(ChannelHandlerContext ctx) throws Exception {
        super.channelRegistered(ctx);
    }
    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        ctx.flush();
    }
    @Override
    public void handlerAdded(ChannelHandlerContext ctx) {  // (2)
        Channel incoming = ctx.channel();
        //nowConnectNum.getAndIncrement(); //自增 （也是添加在线用户）
//        if(nowConnectNum.get() <= connectNum.get()){
        //添加在线用户
        logger.info("Client:"+incoming.remoteAddress() +"加入"+">>>"+ HeartBeatServer.dataTime());
//        }else {
//           // ctx.close();
//            System.out.println("Client连接数超过限制");
//        }

    }
    /**
     * 数据效验 后台服务器的连接消息以：S 开头  设备信息以：$ 开头
     * @param body
     * @return
     */
    public String dataFormatCheck(String body,ChannelHandlerContext ctx){
        String currentTime = "";
        int lenth = body.length();
        //简单的判断协议
        if(!"".equals(body)){
            if(beginS.equals(body.substring(0,1))){
                //后台服务器消息处理
                currentTime = serverCheck(body,ctx);
            }else {
                currentTime = Const.BAD_FORMAT;
            }
        }else {
            currentTime = Const.BAD_ORDER;
        }
        return currentTime;
    }

    /**
     * 否则处理服务器发来的消息
     * @param msg
     * @param ctx
     * @return
     */
    public String serverCheck(String msg,ChannelHandlerContext ctx){
        int lenth = msg.length();
        String currentTime ;

        if(endl.equals(msg.substring(lenth-1,lenth))){
            String news = msg.substring(0,lenth-1);
            //获取开头的设备ID
            String[] txt = news.split(tID);
            //格式为："服务器id : 接收的通道id ：指令"
            if(txt.length<3){
                return Const.BAD_FORMAT;
            }
//            if(ChannelManage.userIdAndChannelMap.get(txt[0])==null){
//                System.err.println("这里");
//                return Const.BAD_FORMAT;
//            }
            Channel channel = (Channel) ChannelManage.userIdAndChannelMap.get(txt[1]);
            channel.writeAndFlush(DevConnectHandler.formatData(txt[2]+end));
            currentTime = Const.SECCESSF;

        }else {
            currentTime = Const.BAD_FORMAT;
        }
        return currentTime;
    }
    public String devCheck(String body,ChannelHandlerContext ctx) {
        int lenth = body.length();
        String currentTime ;
        try{
            if(endl.equals(body.substring(lenth-1,lenth))){
                String news = body.substring(1,lenth-1);
                //获取开头的设备ID
                String[] txt = news.split(tID);
                if(txt.length<3){
                    return Const.F_ERROR;
                }
                //设备未注册直接退出,先注释掉，因为想到，数据发送上来就直接保存数据至redis中，（后面可以添加设备缓存预热，提前预热有哪些设备，就不用每次都查数据库）
//            if(HeartbeatServerHandler.cheackID(txt[0])==0){
//                currentTime = Const.UNREGISTERED;
//                return currentTime;
//            }
                // 先判断通道管理器中是否存在该通道，有就更新 后期可移除这个，设计更好的
                if(ChannelManage.hasChannel(txt[0])){
                    //重新初始化心跳包数为0
                    ChannelManage.userIdHeardMap.put(txt[0],0);
                }
                // 先判断通道管理器中是否存在该通道，没有则添加进去
                else{
                    ChannelManage.userIdAndChannelMap.put(txt[0], ctx.channel());
                    //初始化心跳包数为0
                    ChannelManage.userIdHeardMap.put(txt[0],0);
                }

                //存redis ,保存一星期
                try{
                    redisUtil.set(devName+txt[0],body,60*60*24*7L);
                    currentTime = Const.S_INT;
                    logger.info("保存到redis成功");
                }catch (Exception e){
                    logger.error("保存redis ERROR: "+e.getMessage());
                    currentTime = Const.E_INT;
                }

//            //存储数据库
//            try{
//                currentTime =  ConnectHandler.insertToMysql(news);
//            }catch (Exception e){
//                currentTime = Const.ERROR;
//            }

            }else {
                currentTime = Const.F_ERROR;
            }
        }catch (Exception e){
            currentTime = Const.E_INT;
        }
        return currentTime;
    }
    /**
     * 数据存入数据库
     * @param msg
     * @return
     */
    public static String insertToMysql(String msg){
        String str;
        // 传递sql语句
        Statement stt;
        Connection conn = null;
        String sql = "insert into env_data(datas) values ('"+msg+"')";
        //env_data()一定要连起来不然会有错误
        try {
            conn = MysqlImages.getConn();
            //获取Statement对象
            stt = conn.createStatement();
            //执行sql语句
            stt.executeUpdate(sql);
            System.out.println("<<<>>>插入数据库成功");
            str = Const.SECCESSF;
        } catch (Exception e) {
            System.out.println(">>><<<插入数据错误--"+e.getMessage());
            str = Const.ERROR;
        }finally {
            try {
                conn.close();
            } catch (SQLException e) {
                System.out.println(">>><<<关闭数据库错误"+e.getMessage());
            }
        }
        return str;
    }

}
