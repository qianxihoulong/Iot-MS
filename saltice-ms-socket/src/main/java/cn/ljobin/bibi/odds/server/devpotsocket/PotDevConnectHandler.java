package cn.ljobin.bibi.odds.server.devpotsocket;

import cn.ljobin.bibi.odds.Const;
import cn.ljobin.bibi.odds.inboundhandler.MySimpleChannelInboundHandler;
import cn.ljobin.bibi.odds.server.HeartBeatServer;
import cn.ljobin.bibi.odds.server.SocketChooseHandler;
import cn.ljobin.bibi.odds.service.impl.DataServiceImpl;
import cn.ljobin.bibi.odds.unity.SpringContextByClassUtil;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.util.CharsetUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static cn.ljobin.bibi.odds.server.SocketChooseHandler.POT_TAG;


/**
 * @author liuyanbin
 */
public class PotDevConnectHandler extends MySimpleChannelInboundHandler<Object> {

    private static final Logger logger = LoggerFactory.getLogger(PotDevConnectHandler.class);

    //设备上传的数据消息
    /** $+设备编号+ : +地址+功能+数据位+数据+校验位+# **/
    //后台服务器向设备发送消息
    /** S+设备编号+ : +地址+功能+数据位+数据+校验位+# **/
    /**统计用户发送消息次数**/
    private int counter;
    /**花式注入redis服务，不能用@Autowired注入**/
    private static DataServiceImpl redisUtil;
    static {
        redisUtil = SpringContextByClassUtil.getBean(DataServiceImpl.class);
    }

    public PotDevConnectHandler() {

    }



    @Override
    public void messageRead(ChannelHandlerContext ctx, Object msg) {
        byte[] bytes = (byte[]) msg;
        int len = bytes.length;
        String msDev = SocketChooseHandler.BinaryToHexString(bytes,2,len-2);
        System.err.println(++counter+"-"+ctx.channel().id()+"接收硬件"+
                ctx.channel().remoteAddress()+"数据："+ msDev);
        //判断数据包是否是盆栽的
        if(SocketChooseHandler.BinaryToHexString(bytes[2]).equals(POT_TAG)){
            //接收到心跳包更新心跳包数目为0
            String did = PotHeartbeatServerHandler.getKey(PotChannelManage.userIdAndChannelMap,ctx.channel()).get(0);
            PotChannelManage.userIdHeardMap.put(did,0);
        }else {
            ctx.channel().writeAndFlush(formatData(Const.S_INT));
        }
    }

    /**
     * 统一格式化netty发送的数据格式
     * @param msg
     * @return
     */
    public static Object formatData(String msg){
        return Unpooled
                .unreleasableBuffer(Unpooled.copiedBuffer(msg,
                        CharsetUtil.UTF_8));
    }
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx , Throwable cause){
        logger.error(ctx.channel().id()+"关闭连接"+">>>"+ HeartBeatServer.dataTime());
        PotHeartbeatServerHandler.remove(ctx.channel());
        ctx.close();
    }
    @Override
    public void channelRegistered(ChannelHandlerContext ctx) throws Exception {
        super.channelRegistered(ctx);
    }
    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        ctx.flush();
    }
    @Override
    public void handlerAdded(ChannelHandlerContext ctx) {
        Channel incoming = ctx.channel();
        //添加在线设备
        logger.info("Client:"+incoming.remoteAddress() +"加入"+">>>"+ HeartBeatServer.dataTime());

    }

}
