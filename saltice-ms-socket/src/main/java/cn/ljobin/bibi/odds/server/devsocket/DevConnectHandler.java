package cn.ljobin.bibi.odds.server.devsocket;

import cn.ljobin.bibi.odds.Const;
import cn.ljobin.bibi.odds.inboundhandler.MySimpleChannelInboundHandler;
import cn.ljobin.bibi.odds.server.HeartBeatServer;
import cn.ljobin.bibi.odds.server.SocketChooseHandler;
import cn.ljobin.bibi.odds.server.serversocket.ConnectHandler;
import cn.ljobin.bibi.odds.server.serversocket.ServerChannelManage;
import cn.ljobin.bibi.odds.server.websocket.WebSocketChannelManage;
import cn.ljobin.bibi.odds.service.impl.DataServiceImpl;
import cn.ljobin.bibi.odds.unity.SpringContextByClassUtil;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.util.CharsetUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * @author liuyanbin
 */
public class DevConnectHandler extends MySimpleChannelInboundHandler<Object> {

    private static final Logger logger = LoggerFactory.getLogger(DevConnectHandler.class);

    /**
     * AtomicInteger满足在高并发的情况下,原生的整形数值自增线程不安全的问题
     */
    /**控制最大连接数量**/
    private AtomicInteger connectNum;
    /**当前最大连接数量**/
    private AtomicInteger nowConnectNum;
    //设备上传的数据消息
    /** $+设备编号+ : +地址+功能+数据位+数据+校验位+# **/
    //后台服务器向设备发送消息
    /** S+设备编号+ : +地址+功能+数据位+数据+校验位+# **/
    /**统计用户发送消息次数**/
    private int counter;
    /**花式注入redis服务，不能用@Autowired注入**/
    private static DataServiceImpl redisUtil;
    static {
        redisUtil = SpringContextByClassUtil.getBean(DataServiceImpl.class);
    }

    public DevConnectHandler(AtomicInteger connectNum) {
        this.connectNum = connectNum;
    }



    @Override
    public void messageRead(ChannelHandlerContext ctx, Object msg) {
        byte[] bytes = (byte[]) msg;
        int len = bytes.length;
        String[] msDev = SocketChooseHandler.BinaryToHexString(bytes,2,len-2).split(":");

        System.err.println(++counter+"-"+ctx.channel().id()+"接收硬件"+
                ctx.channel().remoteAddress()+"数据："+ Arrays.toString(msDev));

        int msL = msDev.length;
        //设备联网注册 目标账号
        if(msL == 1){
            //暂时未添加和账号id的绑定
            Channel incoming = ctx.channel();
            incoming.writeAndFlush(formatData(Const.S_INT));
            if(!HeartbeatServerHandler.add(incoming)){
                System.err.println("添加管理出错了");
            }
        }// 非紧急数据处理 目标账号：数据   以及心跳包处理
        else if(msL == 2){
            if("H".equals(msDev[1])){
                //接收到心跳包更新心跳包数目为0
                String did = HeartbeatServerHandler.getKey(ChannelManage.userIdAndChannelMap,ctx.channel()).get(0);
                ChannelManage.userIdHeardMap.put(did,0);
            }else {
                Channel channel = ServerChannelManage.serverIdAndChannelMap.get(ConnectHandler.serverName);
                if (channel == null) {
                    ctx.channel().writeAndFlush(formatData(Const.NO_DEV));
                } else {
                    channel.writeAndFlush(formatData(msDev[0] + ":" + msDev[1] + ConnectHandler.end));
                    ctx.channel().writeAndFlush(formatData(Const.S_INT));
                }
            }
        }//紧急数据处理 server：目标账号：数据  走websocket路线  //暂时没时间改，用于盆栽使用
        else if(msL == 3){
            //通过websocket发送数据
//            String webId = WebSocketChannelManage.userIdAndSocketId.get(msDev[1]);
//            Channel channel = WebSocketChannelManage.webSocketIdAndChannelMap.get(WebSocketChannelManage.webSocket+":"+webId);
//            if(channel == null){
//                ctx.channel().writeAndFlush(formatData(Const.NO_DEV));
//            }else {
//                channel.writeAndFlush(new TextWebSocketFrame(Arrays.toString(msDev)));
                ctx.channel().writeAndFlush(formatData(Const.S_INT));
//            }
        }//紧急数据处理 adiv112233：目标账号：控制信息：当前位置  走websocket路线  com口后台发送数据上来使用
        else if(msL == 4){
            if("-1".equals(msDev[3])){
                //通知保存该channel
                    ChannelManage.userIdAndChannelMap.put(msDev[1],ctx.channel());
                    ChannelManage.userIdAndDidMap.put(msDev[2],msDev[1]);
                    ChannelManage.userIdHeardMap.put(msDev[1],0);
                    ctx.channel().writeAndFlush(formatData(Const.S_INT+System.getProperty("line.separator")));
            }else if("y".equals(msDev[3])||"n".equals(msDev[3])){
                //本地com应用上传的状态信息 y 表示启动成功 n 表示失败
                if("y".equals(msDev[3])){
                    ChannelManage.uidAndStatusMap.put(msDev[2],1);
                }else{
                    ChannelManage.uidAndStatusMap.put(msDev[2],0);
                }
                ctx.channel().writeAndFlush(formatData("State saved successfully"+System.getProperty("line.separator")));
            }else if("updateDivKf".equals(msDev[1])){
                //ms 后台服务器 通知本地应用程序立马发送取下一个药的指令
                if("1".equals(msDev[3])){
                    Channel channel = ChannelManage.userIdAndChannelMap
                            .get(ChannelManage.userIdAndDidMap.get(msDev[2]));
                    if(channel != null && channel.isWritable()){
                        ByteBuf resp = Unpooled.copiedBuffer(("update:1"+System.getProperty("line.separator")).getBytes());
                        channel.writeAndFlush(resp);
                        System.err.println("发送 通知本地应用程序 立马发送取下一个药的指令 成功");
                    }
                }
            } else {
                //通过websocket 向对应的channel发送数据
                String webId = WebSocketChannelManage.userIdAndSocketId.get(msDev[1]);
                Channel channel = WebSocketChannelManage.webSocketIdAndChannelMap.get(WebSocketChannelManage.webSocket+":"+webId);
                if(channel == null){
                    ctx.channel().writeAndFlush(formatData(Const.NO_DEV+System.getProperty("line.separator")));
                }else {
                    channel.writeAndFlush(new TextWebSocketFrame(Arrays.toString(msDev)));
                    ctx.channel().writeAndFlush(formatData(Const.S_INT+System.getProperty("line.separator")));
                }
            }
        }else {
            logger.error(ctx.channel().remoteAddress()+"异常数据，断开连接");
            ctx.channel().writeAndFlush(formatData(Const.F_ERROR+System.getProperty("line.separator")));
            ctx.close();
        }
    }

    /**
     * 统一格式化netty发送的数据格式
     * @param msg
     * @return
     */
    public static Object formatData(String msg){
        return Unpooled
                .unreleasableBuffer(Unpooled.copiedBuffer(msg,
                        CharsetUtil.UTF_8));
    }
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx , Throwable cause){
        logger.error(ctx.channel().id()+"关闭连接"+">>>"+ HeartBeatServer.dataTime());
        HeartbeatServerHandler.remove(ctx.channel());
        ctx.close();
    }
    @Override
    public void channelRegistered(ChannelHandlerContext ctx) throws Exception {
        super.channelRegistered(ctx);
    }
    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        ctx.flush();
    }
    @Override
    public void handlerAdded(ChannelHandlerContext ctx) {
        Channel incoming = ctx.channel();
        //添加在线设备
        logger.info("Client:"+incoming.remoteAddress() +"加入"+">>>"+ HeartBeatServer.dataTime());

    }

}
