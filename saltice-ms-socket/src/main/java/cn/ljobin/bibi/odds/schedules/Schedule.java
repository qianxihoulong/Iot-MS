package cn.ljobin.bibi.odds.schedules;


import cn.ljobin.bibi.odds.server.devsocket.HeartbeatServerHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;

/**
 * @program: MAYUN
 * @description: 定时器任务
 * @author: Mr.Liu
 * @create: 2019-01-20 13:43
 **/
//@Component
public class Schedule {

    private static final Logger logger = LoggerFactory.getLogger(Schedule.class);
    private static final SimpleDateFormat DATA_TIME = new SimpleDateFormat("HH:mm:ss");

    /**
     * 更新redis中黑名单ip
     * 一分钟一次
     */
    //@Scheduled(fixedRate = 1000*60)
    public void report(){
        logger.warn("有没有在认真学习呢！小伙子");
        HeartbeatServerHandler.plaintAll();
    }
//    @Scheduled(fixedRate = 2000*60*30)
//    public void likes(){
//        logger.info("<<你好>>：{}",123);
//    }
}
