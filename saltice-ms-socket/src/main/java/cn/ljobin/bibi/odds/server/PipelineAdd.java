package cn.ljobin.bibi.odds.server;

import cn.ljobin.bibi.odds.Const;
import cn.ljobin.bibi.odds.server.decoder.DevDecoder;
import cn.ljobin.bibi.odds.server.decoder.PotDevDecoder;
import cn.ljobin.bibi.odds.server.decoder.ServerStringDecoder;
import cn.ljobin.bibi.odds.server.devpotsocket.PotDevConnectHandler;
import cn.ljobin.bibi.odds.server.devpotsocket.PotHeartbeatServerHandler;
import cn.ljobin.bibi.odds.server.devsocket.DevConnectHandler;
import cn.ljobin.bibi.odds.server.devsocket.HeartbeatServerHandler;
import cn.ljobin.bibi.odds.server.serversocket.ConnectHandler;
import cn.ljobin.bibi.odds.server.websocket.MyWebSocketHandler;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketFrameAggregator;
import io.netty.handler.stream.ChunkedWriteHandler;
import io.netty.handler.timeout.IdleStateHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @program: nettyTCP
 * @description:
 * @author: Mr.Liu
 * @create: 2019-08-11 11:47
 **/

public class PipelineAdd {
    private static Logger logger = LoggerFactory.getLogger(PipelineAdd.class);

    /**
     *  一般不用addBefore ,这个在一开始的时候不会用到，可能会在第一次之后才会用到（channel上下文通过）
     */

    /**
     * websocket的处理逻辑
     * @param e
     */
    public  void websocketAdd(ChannelHandlerContext e){

        e.pipeline().addLast("http-codec",new HttpServerCodec());
        e.pipeline().addLast("aggregator", new HttpObjectAggregator(65536));
        e.pipeline().addLast("http-chunked", new ChunkedWriteHandler());
        e.pipeline().addLast("WebSocketAggregator",new WebSocketFrameAggregator(65535));
        // 添加具体的处理器。可以addLast（或者addFirst）多个handler，
        // 第一个参数是名字，无具体要求，如果填写null，系统会自动命名。
        e.pipeline().addLast("handler", new MyWebSocketHandler());
        logger.info("webSocket处理添加完毕");
        /**ChannelPipeline通过使用管道的方式来处理请求
         * 第一个配置的管道先处理，然后移交给下一个管道来处理，在每个管道处理中
         * 各个handler可以决定是否继续或中断
         * ChannelPipeline和ChannelHandler机制类似于Servlet和Filter过滤器{@link ChannelPipeline}
         * Netty中的事件分为inbound事件和outbound事件。
         * inbound事件通常由I/O线程触发，例如TCP链路建立事件、链路关闭事件、读事件、异常通知事件等。方法名以file开始{@link ChannelHandlerContext}
         * outbound事件类似于发送、刷新、断开连接、绑定本地地址等关闭channel
         */
    }
    /**
     * 药柜dev的处理逻辑
     * @param e
     */
    public  void devAdd(ChannelHandlerContext e){
        //对于 dev ，设置超时断开
        //e.pipeline().addAfter("s","socket",new LengthFieldBasedFrameDecoder(1024,4,2,3,0));
        //e.pipeline().addLast("s",new LengthFieldBasedFrameDecoder(1024,4,2,-2,0));


        //创建DelimiterBasedFrameDecoder对象，将其加入到ChannelPipeline
        //参数1024表示单条消息的最大长度，当达到该长度仍然没有找到分隔符就抛出TooLongFrame异常，第二个参数就是分隔符
        //由于DelimiterBasedFrameDecoder自动对请求消息进行了解码，下面的ChannelHandler接收到的msg对象就是完整的消息包
        e.pipeline().addLast("work",new DelimiterBasedFrameDecoder(1024, Unpooled.copiedBuffer("@".getBytes())));
        //读\写超时状态处理;
        e.pipeline().addLast("idleStateHandler", new IdleStateHandler(0,
                0, Const.HEARTCHECKINTERVAL, TimeUnit.SECONDS));
        e.pipeline().addLast("heart",new HeartbeatServerHandler());
        e.pipeline().addLast("byte",new DevDecoder());
        e.pipeline().addLast("actives" , new DevConnectHandler(new AtomicInteger(2)));

        logger.info("dev处理添加完毕");
    }
    /**
     * 盆栽dev的处理逻辑
     * @param e
     */
    public  void potDevAdd(ChannelHandlerContext e){
        //对于 dev ，设置超时断开
        //创建DelimiterBasedFrameDecoder对象，将其加入到ChannelPipeline
        //参数1024表示单条消息的最大长度，当达到该长度仍然没有找到分隔符就抛出TooLongFrame异常，第二个参数就是分隔符
        //由于DelimiterBasedFrameDecoder自动对请求消息进行了解码，下面的ChannelHandler接收到的msg对象就是完整的消息包
        e.pipeline().addLast("work",new DelimiterBasedFrameDecoder(1024, Unpooled.copiedBuffer("@".getBytes())));
        //读\写超时状态处理;
        e.pipeline().addLast("idleStateHandler", new IdleStateHandler(60,
                60, Const.HEARTCHECKINTERVAL, TimeUnit.SECONDS));
        e.pipeline().addLast("heart",new PotHeartbeatServerHandler());
        e.pipeline().addLast("byte",new PotDevDecoder());
        e.pipeline().addLast("actives" , new PotDevConnectHandler());

        logger.info("盆栽pot处理添加完毕");
    }
    /**
     * 服务器的Handler管理
     * @param ctx
     */
    public void serverAdd(ChannelHandlerContext ctx){
        //对于 后台系统 ，不设置超时断开
        ctx.pipeline().addLast("serverDecoder",new DelimiterBasedFrameDecoder(1024, Unpooled.copiedBuffer("@".getBytes())));
        ctx.pipeline().addLast("String",new ServerStringDecoder(3));
        //设置连接数
        ctx.pipeline().addLast("connect",new ConnectHandler(new AtomicInteger(2)));
        logger.info("服务器处理添加完毕");
    }
}
