package cn.ljobin.bibi.odds.server;


import cn.ljobin.bibi.odds.Const;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Discards any incoming data.
 * @author liuyanbin
 */
public class HeartBeatServer {
    private int port;
    private AtomicInteger connectNum;

    /**
     * 所有超时
     */
    private int heartCheckInterval ;

    public HeartBeatServer(int port, int heartCheckInterval) {
        this.port = port;
        this.heartCheckInterval = heartCheckInterval;
        //设置最大连接数
        connectNum = new AtomicInteger(2);
    }

    public void run() {
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    // 设置tcp缓冲区
                    .option(ChannelOption.SO_BACKLOG, 1024)
                    // 设置发送缓冲大小
                    .option(ChannelOption.SO_SNDBUF, 32 * 1024)
                    // 这是接收缓冲大小
                    .option(ChannelOption.SO_RCVBUF, 32 * 1024)
                    // 保持连接
                    .option(ChannelOption.SO_KEEPALIVE, true)
                    //快速复用端口
                    .option(ChannelOption.SO_REUSEADDR, true)
                    .option(ChannelOption.CONNECT_TIMEOUT_MILLIS,5000)
                    .childHandler(new HeartBeatServerChannelHandler());

            /**绑定端口并且添加监听和异步启动**/
            ChannelFuture f = b.bind(port).sync();
            f.channel().closeFuture().sync();
        } catch (Exception e) {
            System.out.println(e.getMessage());

        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }
    }

    public static void main(String[] args) {
        int port;
        if (args.length > 0) {
            port = Integer.parseInt(args[0]);
        } else {
            port = 8090;
        }
        System.out.println("Netty服务启动成功：开始监听端口："+port);
        new HeartBeatServer(port, Const.HEARTCHECKINTERVAL).run();
    }
    public static void func(){
        // 先设置一个默认的，要是有其他的端口传进来就是执行下面的代码
        int port = 8090;
        System.out.println("Netty服务启动成功：开始监听端口："+port);
        new HeartBeatServer(port, Const.HEARTCHECKINTERVAL).run();
    }
    public static String dataTime(){
        Date day=new Date();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return df.format(day);
    }
}
