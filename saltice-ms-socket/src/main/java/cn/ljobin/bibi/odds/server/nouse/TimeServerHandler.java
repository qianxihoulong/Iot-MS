package cn.ljobin.bibi.odds.server.nouse;

import cn.ljobin.bibi.odds.Const;
import cn.ljobin.bibi.odds.MysqlImages;
import cn.ljobin.bibi.odds.inboundhandler.MySimpleChannelInboundHandler;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;

import java.net.InetSocketAddress;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @program:
 * @description:
 * @author: Mr.Liu
 * @create: 2019-02-23 17:01
 **/
public class TimeServerHandler extends MySimpleChannelInboundHandler<Object> {

    private AtomicInteger connectNum;

    public TimeServerHandler(AtomicInteger connectNum) {
        this.connectNum = connectNum;
    }

    /** $设备账号:地址+功能+数据位+数据+校验位# **/
    private int counter;

    @Override
    public void messageRead(ChannelHandlerContext ctx,Object msg) throws  Exception{ //msg是服务端接收到客户端的消息
        String body = (String) msg;

        String currentTime;
        //获取客户端ip
        InetSocketAddress ipSocket = (InetSocketAddress)ctx.channel().remoteAddress();
        String clientIp = ipSocket.getAddress().getHostAddress();

        System.out.println("The time server receive order :"+body+"; the counter is :"+ ++counter);

        //数据效验
        currentTime = DataFormatCheck(body);

        //System.getProperty("line.separator") 这也是换行符,功能和"\n"是一致的,但是此种写法屏蔽了 Windows和Linux的区别 ，更保险一些
        currentTime = "<<<echo to "+clientIp+">>>"+currentTime + System.getProperty("line.separator");
        ByteBuf resp = Unpooled.copiedBuffer(currentTime.getBytes());
        ctx.writeAndFlush(resp);
    }
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx , Throwable cause){
        System.out.println(ctx.channel().id()+"关闭连接");
        ctx.close();
    }
    @Override
    public void channelRegistered(ChannelHandlerContext ctx) throws Exception {
        super.channelRegistered(ctx);
        if (connectNum.incrementAndGet() % 100 == 0) {
            System.out.println("current connected" + connectNum.get());
        }
    }

    /**
     * 数据效验
     * @param body
     * @return
     */
    public String DataFormatCheck(String body){
        String currentTime = "";
        int lenth = body.length();
        if("QUERY TIME ORDER".equalsIgnoreCase(body)){
            currentTime = new java.util.Date(
                    System.currentTimeMillis()).toString();
        }else if(!"".equalsIgnoreCase(body)){
            if(body.substring(0, 1).equals("$")){
                if(body.substring(lenth-1,lenth).equals("#")){
                    String news = body.substring(1,lenth-1);
                    try{
                        currentTime =  TimeServerHandler.insertMysql(news);
                    }catch (Exception e){
                        currentTime = Const.ERROR;
                    }
                }else {
                    currentTime = Const.BAD_FORMAT;
                }
            }else {
                currentTime = Const.BAD_FORMAT;
            }
        }else {
            currentTime = Const.BAD_ORDER;
        }
        return currentTime;
    }

    /**
     * 数据存入数据库
     * @param msg
     * @return
     */
    public static String insertMysql(String msg){
        String str;
        // 传递sql语句
        Statement stt;
        Connection conn = null;
        String sql = "insert into env_data(datas) values ('"+msg+"')";
        System.out.println(sql);
        try {
            conn = MysqlImages.getConn();
            //获取Statement对象
            stt = conn.createStatement();
            //执行sql语句
            stt.executeUpdate(sql);
            str = Const.SECCESSF;
        } catch (Exception e) {
            str = Const.ERROR;
        }finally {
            try {
                conn.close();
            } catch (SQLException e) {
            }
        }
        return str;
    }
}
