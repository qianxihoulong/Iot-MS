package cn.ljobin.bibi.odds.server.devsocket;

import cn.ljobin.bibi.odds.Const;
import cn.ljobin.bibi.odds.MysqlImages;
import cn.ljobin.bibi.odds.inboundhandler.MySimpleChannelInboundHandler;
import cn.ljobin.bibi.odds.server.HeartBeatServer;
import cn.ljobin.bibi.odds.server.serversocket.ConnectHandler;
import cn.ljobin.bibi.odds.server.serversocket.ServerChannelManage;
import cn.ljobin.bibi.odds.server.websocket.WebSocketChannelManage;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.util.CharsetUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author liuyanbin
 */
public class HeartbeatServerHandler extends MySimpleChannelInboundHandler<Object> {

    public static long onLineNum = 0L;

    private static Logger logger = LoggerFactory.getLogger(HeartbeatServerHandler.class);
    /**$地址+功能+数据位+数据+校验位#**/

    /**统计用户发送消息次数**/
    private int counter;

    /**
     * Return a unreleasable view on the given ByteBuf
     * which will just ignore release and retain calls.
     * 定义的心跳包类型
     * **/
    private static final ByteBuf HEARTBEAT_SEQUENCE = Unpooled
            //Heartbeat : Please send again
            .unreleasableBuffer(Unpooled.copiedBuffer("HPSA"+System.getProperty("line.separator")+ ConnectHandler.end,
                    CharsetUtil.UTF_8));
    private static final ByteBuf HEARTBEAT_SEQUENCE_2 = Unpooled
            //Heartbeat : Please send again
            .unreleasableBuffer(Unpooled.copiedBuffer("HPSA"+System.getProperty("line.separator"),
                    CharsetUtil.UTF_8));
    private static final ByteBuf HEARTBEAT_CLOSE = Unpooled
            //Closed : Please reconnect
            .unreleasableBuffer(Unpooled.copiedBuffer("CPR"+System.getProperty("line.separator")+ConnectHandler.end,
                    CharsetUtil.UTF_8));
    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt)
            throws Exception {
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent event = (IdleStateEvent) evt;
            String type = "";
            if (event.state() == IdleState.READER_IDLE) {
                type = "read idle";
            } else if (event.state() == IdleState.WRITER_IDLE) {
                type = "write idle";
            } else if (event.state() == IdleState.ALL_IDLE) {
                type = "all idle";
            }
            String dID = getKey(ChannelManage.userIdAndChannelMap,ctx.channel()).get(0);
            int num = ChannelManage.userIdHeardMap.get(dID);
            System.out.println(ctx.channel().remoteAddress() + "超时类型：" + type+">>>"+ HeartBeatServer.dataTime());

            //发送 次心跳包，要是还没有消息就断开连接
            if (num >= Const.HEARD_NUM) {
                //发送上面定义的断开连接心跳包
                ctx.writeAndFlush(HEARTBEAT_CLOSE.duplicate()).addListener(
                        ChannelFutureListener.CLOSE_ON_FAILURE);

                ChannelManage.group.remove(ctx.channel());
                remove(ctx.channel());
                ctx.close();
                System.out.println("强制关闭设备"+ctx.channel().id()+">>>"+ HeartBeatServer.dataTime());
            }else if(num >= 0){
                //发送上面定义的心跳包
                ctx.writeAndFlush(HEARTBEAT_SEQUENCE_2.duplicate()).addListener(
                        ChannelFutureListener.CLOSE_ON_FAILURE);
                num++;
                ChannelManage.userIdHeardMap.put(dID,num);
            }
        } else {
            super.userEventTriggered(ctx, evt);
        }
    }
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
            throws Exception {
        Channel incoming = ctx.channel();
        ChannelManage.group.remove(incoming);

        System.err.println("Client:"+incoming.remoteAddress()+"连接异常"+">>>"+ HeartBeatServer.dataTime());
        //获取客户端ip
        InetSocketAddress ipSocket = (InetSocketAddress)ctx.channel().remoteAddress();
        String clientIp = ipSocket.getAddress().getHostAddress();
        //System.getProperty("line.separator") 这也是换行符,功能和"\n"是一致的,但是此种写法屏蔽了 Windows和Linux的区别 ，更保险一些
        String currentTime = "<<<echo to "+clientIp+">>>"+Const.EXCEPTION+System.getProperty("line.separator");
        ByteBuf resp = Unpooled.copiedBuffer((currentTime+ConnectHandler.end).getBytes());
        ctx.writeAndFlush(resp);
        ctx.close();
        // 当出现异常就关闭连接
        /**cause.printStackTrace();**/
        ChannelManage.removeAll(ctx.channel());
    }
    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        ChannelManage.group.add(ctx.channel());
        ctx.flush();
    }
    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        Channel incoming = ctx.channel();
        System.out.println("Client:"+incoming.remoteAddress()+"掉线"+">>>"+ HeartBeatServer.dataTime());
//        ChannelManage.userIdAndChannelMap.forEach((k,v)->{
//            if(v == incoming && !"".equals(k) && k != null){
//                //可以采用redis ,当前采用数据库更新，高并发的时候不稳定
//                System.out.println("此处更新一下设备状态("+this.getClass().getName()+".chaneelInactive)");
////                if(cheackID(k)>0){
////                    updateToMysql2(k,Const.STOP_STATUS);
////                }
//            }
//        });
        ChannelManage.removeAll(ctx.channel());
        //下面方法太繁琐
//        for(Map.Entry<String,Channel> map: ChannelManage.userIdAndChannelMap.entrySet()){
//            if(map.getValue() == incoming && !"".equals(map.getKey()) && map.getKey() != null){
//                //可以采用redis ,当前采用数据库更新，高并发的时候不稳定
//                if(cheackID(map.getKey())>0){
//                    updateToMysql2(map.getKey(),Const.STOP_STATUS);
//                }
//            }
//        }
        //remove(incoming);
        //plentChannel();
        //plaintAll();
    }
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        Channel incoming = ctx.channel();
        System.err.println("zli");
    }

    /**
     * 添加channel到统一管理  设备用
     * @param ctx
     * @return
     */
    public static boolean add(Channel ctx){

        try {
            if(ChannelManage.group.contains(ctx)){
                return true;
            }
            ChannelManage.group.add(ctx);
            ChannelManage.userIdAndChannelMap.put("dev"+ctx.id().toString(), ctx);
            return true;
        }catch (Exception e){
            System.err.println(e.getMessage());
            return false;
        }
    }
    /**
     * 添加channel到统一管理  服务器用
     * @param ctx
     * @return
     */
    public static boolean serverAdd(Channel ctx){

        try {
            if(ServerChannelManage.serverGroup.contains(ctx)){
                return true;
            }
            ServerChannelManage.serverGroup.add(ctx);
            ServerChannelManage.serverIdAndChannelMap.put(ConnectHandler.serverName, ctx);
            logger.info("服务器channel添加完成");
            return true;
        }catch (Exception e){
            System.err.println(e.getMessage());
            return false;
        }
    }
    /**
     * 添加channel到统一管理  websocket用
     * @param webId
     * @param uid
     * @param ctx
     * @return
     */
    public static boolean websocketAdd(String webId,String uid,Channel ctx){

        try{
            if(WebSocketChannelManage.webSocketGroup.contains(ctx)){
                System.err.println("11");
                return true;
            }
            if(WebSocketChannelManage.userIdAndSocketId.get(uid)!=null){
                System.err.println("22");
                return true;
            }
            WebSocketChannelManage.webSocketGroup.add(ctx);
            WebSocketChannelManage.userIdAndSocketId.put(uid,webId);
            WebSocketChannelManage.webSocketIdAndChannelMap.put(WebSocketChannelManage.webSocket+":"+webId, ctx);
            logger.info("webSocket channel"+ctx.remoteAddress()+" 添加成功==>>{uid: "+uid+",webId: "+webId+"}");
            return true;
        }catch (Exception e){
            System.err.println(e.getMessage());
            return false;
        }
    }
    /**
     * 从统一管理 移除channel
     * @param ctx
     * @return
     */
    public static boolean remove(Channel ctx){
        try {
            ChannelManage.group.remove(ctx);
            for (Map.Entry<String, Channel> entry : ChannelManage.userIdAndChannelMap.entrySet()) {
                String b = entry.getKey();
                Channel s = entry.getValue();
                if (s == ctx) {
                    ChannelManage.userIdAndChannelMap.remove(b, s);
                    break;
                }
            }
            return true;
        }catch (Exception e){
            return false;
        }
    }
    /**
     * 打印剩余的连接用户
     */
    public void plentChannel(){
        System.out.print("还有Client:");
        for (Channel channel : ChannelManage.group) {
            System.out.print("("+channel.remoteAddress()+")");
        }
        System.out.println();
    }

    public static void plaintAll() {
        StringBuilder s = new StringBuilder();
        AtomicLong typeNum = new AtomicLong(0L);
        ChannelManage.group.forEach(v->{
            ChannelManage.userIdAndChannelMap.forEach((k,vs)->{
                if(v==vs){
                    s.append(k).append(",");
                    typeNum.getAndIncrement();
                }
            });
        });
        onLineNum = typeNum.get();
        logger.info("DEV：{name:{},size:{}} <<>> SERVER：{name:{},size:{}} <<>> WEBSOCKET：{name:{},size:{}}",
                ChannelManage.group.name(),ChannelManage.group.size(),
                ServerChannelManage.serverGroup.name(),ServerChannelManage.serverGroup.size(),
                WebSocketChannelManage.webSocketGroup.name(),WebSocketChannelManage.webSocketGroup.size());
        logger.info("SERVER===>>>"+ServerChannelManage.serverIdAndChannelMap.toString());
        logger.info("DEV===>>>"+ChannelManage.userIdAndChannelMap.toString());
        logger.info("WEBSOCKET===>>>"+WebSocketChannelManage.webSocketIdAndChannelMap.toString());
        logger.info("现在在线硬件设备数目："+typeNum+",分别为："+s);
        //太繁琐
//        for(Channel channel: ChannelManage.group){
//            for(Map.Entry<String, Channel> map : ChannelManage.userIdAndChannelMap.entrySet()){
//                if(channel==map.getValue()){
//                    s.append(map.getKey()).append(",");
//                    typeNum.getAndIncrement();
//                }
//            }
//        }
//        String[] sb = s.toString().split(",");
//        for(int i =0;i<sb.length;i++){
//            if(cheackID(sb[i])>0){
//                updateToMysql2(sb[i],Const.RUN_STATUS);
//            }else {
//
//            }
//        }

    }

    /**
     * 通过值 获取 kry
     * @param map
     * @param value
     * @return
     */
    public static List<String> getKey(ConcurrentMap<String,Channel> map, Channel value){
        List<String> keyList = new ArrayList<>();
        for(String key: map.keySet()){
            if(map.get(key).equals(value)){
                keyList.add(key);
            }
        }
        return keyList;
    }
    /**
     * 通过值 获取 kry
     * @param map
     * @param value
     * @return
     */
    public static String getKey2(ConcurrentMap<String,?> map, Object value){
        AtomicReference<String> key = new AtomicReference<>("");
        map.forEach((k,v)->{
            if(v == value){
                key.set(k);
            }
        });
        return key.get();
    }

    /**
     * 数据存入数据库
     * @param msg
     * @return
     */
    public static String updateToMysql2(String id,String msg){
        String str;
        // 传递sql语句
        Statement stt;
        Connection conn = null;
        String sql = "update tb_equipment_list set dev_status = "+msg+" where dev_ID = "+id;
        //env_data()一定要连起来不然会有错误
        try {
            conn = MysqlImages.getConn2();
            //获取Statement对象
            stt = conn.createStatement();
            //执行sql语句
            stt.executeUpdate(sql);
            str = Const.SECCESSF;
        } catch (Exception e) {
            str = Const.ERROR;
        }finally {
            try {
                conn.close();
            } catch (SQLException e) {
            }
        }
        return str;
    }

    /**
     * 检测是否有当前设备
     * @param id
     * @return
     */
    public static int cheackID(String id){
        int str;
        Statement stt;
        Connection conn = null;
        ResultSet rs = null;
        String sql = "select * from tb_equipment_list where dev_ID = "+id;
        try {
            conn = MysqlImages.getConn2();
            stt = conn.createStatement();
            rs = stt.executeQuery(sql);
            if(rs.next()){
                str=1;
            }else {
                str=0;
            }
        } catch (Exception e) {
            str = 0;
        }finally {
            try {
                conn.close();
            } catch (SQLException e) {
            }
        }
        return str;
    }

    @Override
    protected void messageRead(ChannelHandlerContext ctx, Object o) throws Exception {

    }
}
