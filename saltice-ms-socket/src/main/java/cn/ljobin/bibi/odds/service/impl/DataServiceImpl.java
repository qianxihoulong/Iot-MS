package cn.ljobin.bibi.odds.service.impl;

import cn.ljobin.bibi.common.RedisUtil;
import cn.ljobin.bibi.odds.service.DataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @program: nettyTCP
 * @description:
 * @author: Mr.Liu
 * @create: 2019-08-04 20:20
 **/
public class DataServiceImpl implements DataService {
    private static Logger logger = LoggerFactory.getLogger(DataServiceImpl.class);
    @Autowired
    RedisUtil redisUtil;
    /**
     * 数据存储redis
     * @param msg   K
     * @param value  V
     */
    @Override
    public void set(String msg, Object value) {
        try{
            redisUtil.set(msg,value);
        }catch (Exception e){
            logger.error("插入redis失败"+e.getMessage());
        }
    }
    /**
     * 数据存储redis
     * @param msg   K
     * @param value  V
     * @param time 保存时间
     */
    @Override
    public void set(String msg, Object value, Long time) {
        try{
            redisUtil.set(msg,value,time);
        }catch (Exception e){
            logger.error("插入redis失败"+e.getMessage());
        }
    }
}
