package cn.ljobin.bibi.odds.service;

/**
 * @program: nettyTCP
 * @description:
 * @author: Mr.Liu
 * @create: 2019-08-04 20:19
 **/
public interface DataService {
    /**
     * 数据存储redis
     * @param msg   K
     * @param value  V
     */
    public void set(String msg, Object value);

    /**
     * 数据存储redis
     * @param msg   K
     * @param value  V
     * @param time 保存时间
     */
    public void set(String msg, Object value, Long time);
}
