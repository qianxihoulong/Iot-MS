package cn.ljobin.bibi.odds.server.devsocket;

import cn.ljobin.bibi.odds.server.serversocket.ConnectHandler;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * 存储整个工程的全局配置
 * @author liuyazhuang
 *
 */
public class ChannelManage {

    private static final Logger logger = LoggerFactory.getLogger(ChannelManage.class);
	/**
	 * 存储每一个硬件客户端接入进来时的channel对象
	 */
	public static ChannelGroup group = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

	/**读锁**/
    private static ReentrantReadWriteLock rwLock = new ReentrantReadWriteLock(true);

    /**存储连接中的硬件设备 <设备编号 对应的Channel/>**/
    public static ConcurrentMap<String, Channel> userIdAndChannelMap = new ConcurrentHashMap<>();
    /**存储连接中的硬件设备 <设备编号/账号 对应的设备DID>**/
    public static ConcurrentMap<String, String> userIdAndDidMap = new ConcurrentHashMap<>();
    /**存储连接中的硬件设备 <uid 对应的设备状态>**/
    public static ConcurrentMap<String, Integer> uidAndStatusMap = new ConcurrentHashMap<>();
    /**心跳包限制数 <设备编号 对应的当前发送过的心跳包数/>**/
    public static ConcurrentMap<String, Integer> userIdHeardMap = new ConcurrentHashMap<>();

    /**
     * 移除该channel的所有保存
     * @param channel
     */
    public static void removeAll(Channel channel){
        group.remove(channel);
        String did =  HeartbeatServerHandler.getKey2(userIdAndChannelMap,channel);
        System.err.println(did);
        if(!"".equals(did)){
            userIdAndChannelMap.remove(did);
            String uid = HeartbeatServerHandler.getKey2(userIdAndDidMap,did);
            userIdAndDidMap.remove(uid);
            uidAndStatusMap.remove(uid);
            userIdHeardMap.remove(did);
            ;
        }


    }
	public static void send(String senderId, String receiverId, String message, Channel senderChannel) {
	    // 发送肯定是A要给B发，A就是发消息的对象，B可以是人，机器等对象
        try {
            rwLock.readLock().lock();
            // 1.寻找receiverId的channel
            Channel receiverChannel = userIdAndChannelMap.get(receiverId);
            if (receiverChannel==null){
                // 使用发送者的通道告知发送者，你要发的那个人不在线
                senderChannel.writeAndFlush(Unpooled.copiedBuffer((receiverId + "离线中..."+ ConnectHandler.end).getBytes()));
                return;
            }
            // 2.发送。B若要收到消息，其实是通过B的channel给B发消息
            //new TextWebSocketFrame(senderId + "发来消息===》" + message)
            ByteBuf resp = Unpooled.copiedBuffer((message+ConnectHandler.end).getBytes());
            receiverChannel.writeAndFlush(resp);
            System.out.println(senderId+":发送给："+receiverId+"消息："+message);
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        } finally {
            rwLock.readLock().unlock();
        }
    }

    public static boolean hasChannel(String id) {
        Channel channel = userIdAndChannelMap.get(id);
        if (channel == null) {
            return false;
        } else {
            return true;
        }
    }

}
